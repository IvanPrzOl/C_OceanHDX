////////////////////////////////////////////////////////////////////////////////
// There are 4 APIs for communicating with Ocean spectrometers:
// - Omnidriver: recommended version, in Java, but C/C++ compatible. 
//               Difficult to install, buggy, crashes all the time with wrong versions of Java and 
//               does not recognizes the Ocean OceanHDX
// - SeaBreeze new and old version: open source, neither recognises the HDX as of 2019
// - Firmware: based on "Ocean HDX Firmware and Advanced Communications Rev2.pdf". This is what is implemented here
//             This library deals with only one spectrometer at a time
// Author:            Guillaume Dargaud - https://www.gdargaud.net/Hack/OceanSpectro.html
// Open source GPLv3: https://gitlab.com/dargaud/C_OceanHDX
////////////////////////////////////////////////////////////////////////////////

#include <iso646.h>
#include <stdlib.h>		// exit
#include <stdio.h>		// printf
#include <string.h>		// strlen
#include <stdarg.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <assert.h>
#include <stddef.h>		// offsetof
#include <ctype.h>		// iscntrl
#include <unistd.h>		// close
#include <signal.h>		// Ctrl-C trap

// For ethernet communication
#include <sys/socket.h>	// socket, Linux-specific
#include <arpa/inet.h>	// inet_addr
//#include <netinet/in.h>

// For USB communication
#include <libusb-1.0/libusb.h>	// You need libusb-1.0, link with -lusb-1.0

#include "UseColors.h"
#include "OceanHDX.h"

#define MAX_REPLY (2*50000)		// NOTE: The max size is supposedly given by GetMaxBufferSize (which returns 50000, firmware bug ???),
								// but a RAW buffer of 15 scans is 63060. So we allocate conservatively here.
								// My guess is that the limit is 0xFFFF, but 16 scans of 4204 won't fit, so 15 is the limit

static int sock=0;							// Ethernet socket or...
struct libusb_device_handle *handle = NULL;	// ...USB handle of spectrometer (only one at a time)

static int Display=D_ALL;		// Global to skip displaying some commands or replies
void OceanHDX_Debug(int Disp) { Display=Disp; }

#define msleep(ms) usleep((ms)*1000)// ms sleep

static char LastErrMsg[1024]="";

typedef struct sOBPh {			// Ocean Binary Protocol HEADER
	unsigned char StartBytes[2];
	unsigned short ProcotolVersion, Flags, ErrorNumber;
	unsigned int MessageType, Regarding;
	unsigned char Reserved[6], ChecksumType, ImmediateDataLen, ImmediateData[16];
	unsigned int BytesRemaining;
} __attribute__((packed)) tOBPh;

typedef struct sOBPf {			// Ocean Binary Protocol FOOTER
	unsigned char Checksum[16], Footer[4];
} __attribute__((packed)) tOBPf;

static const tOBPh DefaultHeader={	{0xC1, 0xC0},
							0, 0, 0,
							0, 0,
							{0}, 0, 0, {0},
							0
};

static const tOBPf DefaultFooter={ {0}, {0xC5, 0xC4, 0xC3, 0xC2} };
// In between header and footer is a variable size payload

static int NoImmediate=0;		// When constructing a packet, disable use of immediate data even if size <16

typedef struct sMetadata {
	unsigned short Protocol;			// Default 0x0002
	unsigned short Length;				// Default xx0040
	unsigned int TotalPixelDataLength;	// Default 0x000010B0
	union { struct { unsigned int CounterLSW, CounterMSW; };	// in us
			long long TimeUS; };		// in time_t*1000000
	unsigned int IntegrationTime;		// default 0x0000000A, in u
	unsigned int PixelDataFormat;		// 0x00000001:U16 (Default), 0x00000002:U24, 0x00000003:U32, 0x00000004:SPFP
	unsigned int SpectrumCount;			// This value increments by one for each spectrum acquired since last reset. 
										// With scan averaging this is the first spectrum index.
	unsigned int LastSpectrumCount;		// When scan averaging is active, this value represents the last spectrum index. (Added by i.MX firmware.)
	unsigned int LastCounterLSW, LastCounterMSW;	// Microseconds for last spectrum taken for a scan averaged set. (Added by i.MX firmware.)
	unsigned short ScansToAverage;		// The number of spectra averaged together in the result. (Added by i.MX firmware.)
	unsigned short Reserved;
	unsigned int Reserved_[5];
} __attribute__((packed)) tMetadata;

///////////////////////////////////////////////////////////////////////////////
static void Lambda(void);
static int DisplayRawMessage(const unsigned char* Buffer, int Size);

static void (*PreviousSignalHandlerPipe)(int) = NULL;
static void (*PreviousSignalHandlerInt )(int) = NULL;

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Trap handler to disable drives and close sockets properly
////////////////////////////////////////////////////////////////////////////////
static void intHandler(int Signal) {
	if (Signal==SIGPIPE) puts(BLD RED "\n*** Broken pipe - Closing OceanHDX ***\n" NRM);
	if (Signal==SIGINT ) puts(BLD RED "\n*** Abort - Closing OceanHDX ***\n" NRM);
	OceanHDX_Close();
	if (Signal==SIGPIPE and PreviousSignalHandlerPipe) PreviousSignalHandlerPipe(Signal);
	if (Signal==SIGINT  and PreviousSignalHandlerInt)  PreviousSignalHandlerInt (Signal);
	exit(2);
}

///	HIFN	Ensure that the binary structures are packed and match the firmware
static void Verif(void) {
	assert(sizeof(tOBPh)==44);
	assert(sizeof(tOBPf)==20);
	assert(sizeof(tMetadata)==64);
	assert(sizeof(tSpectroPixels)==(PIXEL_NB+20)*sizeof(short));
	assert(sizeof(tSpectroPixels)==SIZE_OF_DATA);
	assert(sizeof(time_t)==8);	// This is not important, only for the timestamp conversions
}

static int PurgeBuffer(void);
	
////////////////////////////////////////////////////////////////////////////////
/// HIFN    Initializes socket communication
/// HIFN	Note, this device also accepts USB (implemented below), serial and Wifi communication, unimplemented here
/// HIRET	0 if no error
////////////////////////////////////////////////////////////////////////////////
int OceanHDX_InitEth(char* Address, int Port) {
	struct sockaddr_in server;
	Verif();
	
	// Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		sprintf(LastErrMsg, "Could not create socket. Error: %s", strerror(errno));
		printf(BLD RED "%s\n" NRM, LastErrMsg);
		return errno;
	}
	printf("Socket created for OceanHDX\n");

	// Prepare the sockaddr_in structure
	server.sin_addr.s_addr = inet_addr(Address);
	server.sin_family = AF_INET;
	server.sin_port = htons( Port );

	// Connect to remote server
	if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) {
		sprintf(LastErrMsg, "Connect to %s:%d failed. Error: %s", Address, Port, strerror(errno));
		printf(BLD RED "%s\n" NRM, LastErrMsg);
		return errno;
	}
	// We could also use bind() and check on EADDRINUSE, but it's more complicated

	// PurgeBuffer();	// Rarely necessary if you exit cleanly. And will block if buffer is empty...
	
	PreviousSignalHandlerPipe=signal(SIGPIPE, intHandler);	// Note: when used as a lib we can't have multiple of those. See https://stackoverflow.com/questions/17102919/is-it-valid-to-have-multiple-signal-handlers-for-same-signal
	PreviousSignalHandlerInt =signal(SIGINT,  intHandler);   // Ctrl-C
	//	printf("Previous handlers: pipe=%p, int=%p\n", PreviousSignalHandlerPipe, PreviousSignalHandlerInt);
	
	printf(BLD GRN "OceanHDX connected\n" NRM);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Initializes USB communication
/// HIPAR	VID/Vendor ID, Use lsusb to get this info
/// HIPAR	PID/Product ID
/// HIRET	0 if no error
////////////////////////////////////////////////////////////////////////////////
int OceanHDX_InitUSB(int VID, int PID) {
	#define MAX_STR 256
	int R=0;
	Verif();
	errno=0;

	if ((R=libusb_init(NULL))) { printf(BLD RED "init: %s / %s\n" NRM, libusb_strerror(R), errno ? strerror(errno) : ""); return errno; }

	//	libusb_set_option(NULL, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_DEBUG);
	
	// NOTE: error handling on libusb is complex. It sometimes sets errno while actually working, like here
	handle = libusb_open_device_with_vid_pid(NULL, VID, PID);
	if (handle==NULL) { printf(BLD RED "open: %s\n" NRM, strerror(errno)); return errno; }
	if (errno and errno!=EAGAIN) printf(BLD RED "open: %s\n" NRM, strerror(errno));
	errno=0;
	
	if ((R=libusb_set_auto_detach_kernel_driver(handle, 1))) { printf(BLD RED "detach: %s / %s\n" NRM, libusb_strerror(R), errno ? strerror(errno) : ""); /*return errno;*/ }
	errno=0;

	PurgeBuffer();
	
	PreviousSignalHandlerPipe=signal(SIGPIPE, intHandler);	// Note: when used as a lib we can't have multiple of those. See https://stackoverflow.com/questions/17102919/is-it-valid-to-have-multiple-signal-handlers-for-same-signal
	PreviousSignalHandlerInt =signal(SIGINT,  intHandler);   // Ctrl-C
	//	printf("Previous handlers: pipe=%p, int=%p\n", PreviousSignalHandlerPipe, PreviousSignalHandlerInt);
	
	printf(BLD GRN "OceanHDX connected\n" NRM);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Shut down the system. You will need to call init again
////////////////////////////////////////////////////////////////////////////////
int OceanHDX_Close(void) {
	if (sock) {
		// Possibly send Abort or other cleanup command here
		close(sock); sock=0;
	} else if (handle) {
//		libhid_close(handle); handle=NULL;
		libusb_close(handle); handle=NULL; libusb_exit(NULL);
//		close(handle); handle=0;
//		fclose(handle); handle=NULL;
	}
	printf(BLD YEL "=========================== OCEAN HDX SPECTRO CLOSED ==========================\n" NRM);
	fflush(NULL);
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
char* OceanHDX_LastErrMsg(void) {
	return LastErrMsg;
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN    Send a command to the spectro
/// HIPAR	Command/Contains the command (header+payload+footer)
/// HIPAR	Reply/Contains the reply (header+payload+footer). Must have been allocated before the call
/// HIPAR	ReplySize/Size of received packet. Must allow for enough transmission time. Pass NULL if you do not want a reply
/// HIRET	Error code. errno is set if socket error
////////////////////////////////////////////////////////////////////////////////
static int SendCommand(/*const*/ unsigned char* Command, int CommandSize, unsigned char* Reply, int *ReplySize) {
	int Transfered=0, N=0;
	errno=0;

	#define USBW(h, buf, nb) libusb_bulk_transfer((h), 0x01, (buf), (nb), &Transfered, 0)
	#define USBR(h, buf, nb) libusb_bulk_transfer((h), 0x81, (buf), (nb), &Transfered, 1)	// Small timeout (1ms). Using 0 will block

	if (sock   and (N=send(sock,   Command, CommandSize, 0)) <= 0 or
		handle and (N=USBW(handle, Command, CommandSize   )) < 0) {
			printf(BLD RED "Send failed. Error %d: %s\n" NRM, N, strerror(errno));
			return errno;
	}
	if (N==0 and Transfered) N=Transfered;	// libusb
	if (N!=CommandSize) printf(BLD RED "N!=CommandSize: %s\n" NRM, strerror(errno));
	else if (errno and errno!=EAGAIN) // Timeout
		printf(BLD RED "Send error: %s\n" NRM, strerror(errno));
	errno=0;
	if (Display!=D_NONE) printf("Raw sending:  "), DisplayRawMessage(Command, CommandSize);

	if (Reply==NULL or ReplySize==NULL) return 0;	// no reply wanted
	
	*ReplySize=0;
	unsigned char* Rep=Reply;
	// Receive a reply from the server - We loop until we have enough bytes to read the reply size, even if we get EAGAIN (timeout)
	do {
		msleep(1);								// Not much needed here
		if (sock   and (N=read(sock,   Rep, MAX_REPLY-*ReplySize))<0 or
			handle and (N=USBR(handle, Rep, MAX_REPLY-*ReplySize))<0) {
			if (errno!=EAGAIN) { printf(BLD RED "Recv failed. Error: %s\n" NRM, strerror(errno)); return errno; }
			N=0;
		}
		if (N==0 and Transfered) N=Transfered;	// libusb only
		if (errno and errno!=EAGAIN)			// Timeout (data not ready yet)
			printf(BLD RED "Read error: %s\n" NRM, strerror(errno));
		else errno=0;
		Rep+=N;
		if (N) printf("*** Read %d bytes", N);
	} while ((*ReplySize+=N)<sizeof(tOBPh));
	
	// We now know the expected size of the data and we loop until we get it all, even if we get EAGAIN (timeout)
	int Expecting=((tOBPh*)Reply)->BytesRemaining+sizeof(tOBPh);
	printf(", Remain:%d, Expect:%d%s", ((tOBPh*)Reply)->BytesRemaining, Expecting, *ReplySize<Expecting?", incoming: ":"");
	while (*ReplySize<Expecting) {
		msleep(1);
		if (sock   and (N=read(sock,   Rep, MAX_REPLY-*ReplySize))<0 or
			handle and (N=USBR(handle, Rep, MAX_REPLY-*ReplySize))<0) {
			if (errno!=EAGAIN) { printf(BLD RED "Recv failed. Error: %s\n" NRM, strerror(errno)); return errno; }
			N=0;
		}
		if (N==0 and Transfered) N=Transfered;	// libusb only
		if (errno and errno!=EAGAIN) 			// Timeout
			printf(BLD RED "Read error: %s\n" NRM, strerror(errno));
		else errno=0;
		if (N) printf(" %d", N);
		*ReplySize+=N;
		Rep+=N;
	}
	printf(", Final:%d ***\n", *ReplySize);

	if (Display==D_ALL)	printf("Raw receiving:"), DisplayRawMessage(Reply, *ReplySize);

	msleep(0);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Read data in the queue and dumps it
/// HIFN	This doesn't seem necessary with ethernet, but it is with USB
/// HIRET	Error code
////////////////////////////////////////////////////////////////////////////////
static int PurgeBuffer(void) {
	int Transfered=0, N=0;
	errno=0;
	
	static unsigned char Rep[MAX_REPLY];
	// EAGAIN when there's a timeout (1ms on read, see USRB macro)
	do {
		msleep(1);								// Not much needed here
		if (sock   and (N=read(sock,   Rep, MAX_REPLY))<0 or
			handle and (N=USBR(handle, Rep, MAX_REPLY))<0) {
			if (errno!=EAGAIN) { printf(BLD RED "Purge failed. Error: %s\n" NRM, strerror(errno)); return errno; }
			N=0;
		}
		if (N==0 and Transfered) N=Transfered;	// libusb
		if (errno and errno!=EAGAIN) 			// Timeout, no data
			printf(BLD RED "Purge error: %s\n" NRM, strerror(errno));
		else errno=0;
		printf("Purged %d bytes\n", N);
	} while (N>0);

	msleep(0);
	return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN    Explain signification of flags
////////////////////////////////////////////////////////////////////////////////
static char* FlagsMsg(int Flags) {
	static char Str[256]=""; *Str='\0';
	if ((Flags>>0)&1) { if (*Str) strcat(Str, ", "); strcat(Str, "Response"); }
	if ((Flags>>1)&1) { if (*Str) strcat(Str, ", "); strcat(Str, "ACK"); }
	if ((Flags>>2)&1) { if (*Str) strcat(Str, ", "); strcat(Str, "Ack REQ"); }
	if ((Flags>>3)&1) { if (*Str) strcat(Str, ", "); strcat(Str, "NACK"); }
	if ((Flags>>4)&1) { if (*Str) strcat(Str, ", "); strcat(Str, "Exception"); }
	if ((Flags>>5)&1) { if (*Str) strcat(Str, ", "); strcat(Str, "Deprecated protocol"); }
	if ((Flags>>6)&1) { if (*Str) strcat(Str, ", "); strcat(Str, "Deprecated message"); }
	return Str;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Explain signification of error number
////////////////////////////////////////////////////////////////////////////////
static char* ErrorMsg(int ErrorNumber) {
	switch (ErrorNumber) {
		case   0: return "Success";
		case   1: return "Bad protocol";
		case   2: return "Bad message type";
		case   3: return "Bad checksum";
		case   4: return "Message too large";
		case   5: return "Invalid Payload length";
		case   6: return "Invalid payload data";
		case   7: return "Device not ready";
		case   8: return "Unknown checksum type";
		case   9: return "Device reset";
		case  10: return "Too many buses";
		case  11: return "Out of memory";
		case  12: return "Value not found";
		case  13: return "Device fault";
		case  14: return "Bad footer";
		case  15: return "Request interupted";
		case  16: return "I/O error";
		case 100: return "Bad cipher";
		case 101: return "Bad firmware";
		case 102: return "Incorrect packet length";
		case 103: return "Wrong hardware revision";
		case 104: return "Wrong firmware revision";
		case 254: return "Proceed";
		case 255: return "Deferred";
		default : return "UNKNOWN ERROR NUMBER";
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Create a colored string with error message
////////////////////////////////////////////////////////////////////////////////
static char* DisplayFlagErr(const tOBPh* Header) {
	static char Str[256]="";
	if ((Header->Flags>>3)&1 /*NACK*/ or (Header->Flags>>4)&1 /*Exception*/)
		sprintf(Str, BLD RED "Flags:%s - Err:%s" NRM, FlagsMsg(Header->Flags), ErrorMsg(Header->ErrorNumber));
	else if (Header->Flags>7 /*Errors*/) 
		sprintf(Str, BLD RED "Flags:%s" NRM, FlagsMsg(Header->Flags));
	else sprintf(Str, "Flags:%s", FlagsMsg(Header->Flags));
	return Str;
}

////////////////////////////////////////////////////////////////////////////////
#define PREFIX_RESET					       0
#define PREFIX_RESET_RESTORE			       1
#define PREFIX_GET_RECOVERY_STATUS		    0x81
#define PREFIX_GET_MAIN_FIRMWARE_REV	    0x90
#define PREFIX_GET_SECOND_FIRMWARE_REV	    0x91
#define PREFIX_GET_MAIN_FIRMWARE_SUB	    0x92
#define PREFIX_GET_MAIN_SERIAL			   0x100
#define PREFIX_GET_MAIN_SERIAL_MAX_LEN	   0x101
#define PREFIX_GET_DEV_ALIAS			   0x200
#define PREFIX_GET_DEV_ALIAS_MAX_LEN	   0x201
#define PREFIX_SET_DEV_ALIAS			   0x210
#define PREFIX_GET_USER_STR_COUNT		   0x300
#define PREFIX_GET_USER_STR_LEN			   0x301
#define PREFIX_GET_USER_STR				   0x302
#define PREFIX_SET_USER_STR				   0x310
#define PREFIX_GET_TIME					   0x400
#define PREFIX_SET_TIME					   0x410

#define PREFIX_RS232					   0x800	// Unimplemented
#define PREFIX_NETWORK					   0x900	// Unimplemented
#define PREFIX_IP						   0xA00	// Unimplemented

//#define PREFIX_USB					   0xE00	// Unimplemented except for the following:
#define PREFIX_GET_ORIGINAL_VID			   0xE00
#define PREFIX_GET_ORIGINAL_PID			   0xE01
#define PREFIX_GET_ORIGINAL_MANUF_STR	   0xE10
#define PREFIX_GET_ORIGINAL_DEV_STR		   0xE11

#define PREFIX_SET_STATUS_LED			  0x1010	// Undocumented and does not work
#define PREFIX_REPROGRAMMING_MODE		 0xFFF00	// Undocumented and unimplemented and DANGEROUS.
													// If you have to do this, use the official tools

#define PREFIX_GET_PAYLOAD				 0xFF100
#define PREFIX_ECHO_PAYLOAD				 0xFF101
#define PREFIX_ABORT					0x100000

#define PREFIX_GET_BUF_ENABLED			0x100800
#define PREFIX_SET_BUF_ENABLED			0x100810
#define PREFIX_GET_MAX_BUF_SIZE			0x100820
#define PREFIX_GET_CURRENT_BUF_SIZE		0x100822
#define PREFIX_CLEAR_BUF_SPECTRA		0x100830
#define PREFIX_REMOVE_OLDEST_SPECTRA	0x100831
#define PREFIX_SET_CURRENT_BUF_SIZE		0x100832
#define PREFIX_GET_NB_SPECTRA			0x100900
#define PREFIX_GET_RAW_SPECTRUM_META	0x100980
#define PREFIX_GET_SPECTRUM				0x101000
#define PREFIX_GET_MAX_ADC_COUNT		0x101010

#define PREFIX_GET_INTG_TIME			0x110000
#define PREFIX_GET_MIN_INTG_TIME		0x110001
#define PREFIX_GET_MAX_INTG_TIME		0x110002
#define PREFIX_GET_INTG_TIME_STEP		0x110003
#define PREFIX_SET_INTG_TIME			0x110010

#define PREFIX_GET_TRIGGER_MODE			0x110100
#define PREFIX_GET_NB_SPECTRA_PER_TRIG	0x110102
#define PREFIX_SET_TRIGGER_MODE			0x110110
#define PREFIX_SET_NB_SPECTRA_PER_TRIG	0x110112
#define PREFIX_BINNING					0x110280	// Undocumented and unimplemented

#define PREFIX_GET_LAMP_ENABLE			0x110400
#define PREFIX_SET_LAMP_ENABLE			0x110410

#define PREFIX_GET_ACQ_DELAY			0x110500
#define PREFIX_GET_MIN_ACQ_DELAY		0x110501
#define PREFIX_GET_MAX_ACQ_DELAY		0x110502
#define PREFIX_GET_ACQ_DELAY_STEP		0x110503
#define PREFIX_SET_ACQ_DELAY			0x110510

#define PREFIX_GET_SCANS_TO_AVG			0x120000
#define PREFIX_SET_SCANS_TO_AVG			0x120010
#define PREFIX_BOXCAR					0x121000	// Undocumented and unimplemented

#define PREFIX_GET_WL_COEF_COUNT		0x180100
#define PREFIX_GET_WL_COEF				0x180101
#define PREFIX_SET_WL_COEF				0x180111	// Undocumented but necessary for calibration
#define PREFIX_GET_NL_COEF_COUNT		0x181100
#define PREFIX_GET_NL_COEF				0x181101
#define PREFIX_IRRADIANCE				0x182000	// Undocumented and unimplemented
#define PREFIX_GET_SL_COEF_COUNT		0x183100
#define PREFIX_GET_SL_COEF				0x183101

#define PREFIX_GET_HOT_PIXELS			0x186000	// Undocumented and unimplemented - See quick test in main()
#define PREFIX_SET_HOT_PIXELS			0x186010	// Undocumented and unimplemented
#define PREFIX_GET_BENCH_ID				0x1B0000	// Undocumented and unimplemented
#define PREFIX_GET_BENCH_SN				0x1B0100	// Undocumented and unimplemented
#define PREFIX_GET_SLIT_WIDTH			0x1B0200	// Undocumented and unimplemented
#define PREFIX_GET_FIBER_DIAM			0x1B0300	// Undocumented and unimplemented
#define PREFIX_GET_GRATING				0x1B0400	// Undocumented and unimplemented
#define PREFIX_GET_FILTER				0x1B0500	// Undocumented and unimplemented
#define PREFIX_GET_COATING				0x1B0600	// Undocumented and unimplemented


#define PREFIX_GPIO						0x200000	// Unimplemented
#define PREFIX_SINGLE_STROBE			0x300000	// Unimplemented
#define PREFIX_CONTINUOUS_STROBE		0x310000	// Unimplemented

#define PREFIX_GET_NB_TEMPS				0x400000
#define PREFIX_GET_TEMP					0x400001
#define PREFIX_GET_ALL_TEMPS			0x400002

#define PREFIX_SPI						0x500000	// Unimplemented
#define PREFIX_I2C						0x600000	// Unimplemented

#define PREFIX_GET_BATTERY_LEVEL		0x1D0000	// Doesn't seem to work
#define PREFIX_GET_BATTERY_STATUS		0x1D0001	// Doesn't seem to work
#define PREFIX_GET_BATTERY_TEMP			0x1D0002	// Doesn't seem to work

// There are several other prefixes found in the documentation of other spectrometers that would probably work here,
// but since they are not important in most cases, I left them unimplemented, except for PREFIX_SET_WL_COEF
// For instance reprogramming mode to reflash the firmware, pixel binning, boxcar, irradiance


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return a string with the message type explained
////////////////////////////////////////////////////////////////////////////////
static char* MessageTypeStr(int MessageType) {
	switch (MessageType) {
		case PREFIX_RESET					: return "Reset";
		case PREFIX_RESET_RESTORE			: return "Reset and restore";
		case PREFIX_GET_RECOVERY_STATUS		: return "Get recovery status";						// Out U8
		case PREFIX_GET_MAIN_FIRMWARE_REV	: return "Get main firmware revision";				// Out U16 BCD
		case PREFIX_GET_SECOND_FIRMWARE_REV	: return "Get secondary firmware revision";			// Out U16 BCD
		case PREFIX_GET_MAIN_FIRMWARE_SUB	: return "Get main firmware sub-revision";			// Out U16 BCD
		case PREFIX_GET_MAIN_SERIAL			: return "Get main serial number";					// Out Byte array
		case PREFIX_GET_MAIN_SERIAL_MAX_LEN	: return "Get main serial number maximum length";	// Out U8
		
		case PREFIX_GET_DEV_ALIAS			: return "Get device alias";						// Out byte array
		case PREFIX_GET_DEV_ALIAS_MAX_LEN	: return "Get device alias maximum length";			// Out U8
		case PREFIX_SET_DEV_ALIAS			: return "Set device alias";						// In byte array
		
		case PREFIX_GET_USER_STR_COUNT		: return "Get user string count";
		case PREFIX_GET_USER_STR_LEN		: return "Get length of each user string";
		case PREFIX_GET_USER_STR			: return "Get user string";							// Out byte array
		case PREFIX_SET_USER_STR			: return "Set user string";							// Out byte array
		case PREFIX_SET_STATUS_LED			: return "Set status LED";							// In U8[2]
		case PREFIX_REPROGRAMMING_MODE		: return "Put device in reprogramming mode";

		case PREFIX_GET_TIME				: return "Get current time";						// Out U64
		case PREFIX_SET_TIME				: return "Set current time";						// In U64
		
		case PREFIX_RS232					: return "RS232";
		case PREFIX_NETWORK					: return "Network";
		case PREFIX_IP						: return "IP";
//		case PREFIX_USB						: return "USB";
		case PREFIX_GET_ORIGINAL_VID		: return "Get original VID";
		case PREFIX_GET_ORIGINAL_PID		: return "Get original PID";
		case PREFIX_GET_ORIGINAL_MANUF_STR	: return "Get original manufacturer string";
		case PREFIX_GET_ORIGINAL_DEV_STR	: return "Get original device string";
		
		case PREFIX_GET_PAYLOAD				: return "Get payload of length N";					// In U32, Out U8[]
		case PREFIX_ECHO_PAYLOAD			: return "Echo payload";							// In U8[], Out U8[]
		
		case PREFIX_ABORT					: return "Abort acquisition";
		case PREFIX_GET_BUF_ENABLED			: return "Get buffer enabled";						// Out U8
		case PREFIX_SET_BUF_ENABLED			: return "Set buffer enabled";						// In U8
		case PREFIX_GET_MAX_BUF_SIZE		: return "Get max buffer size";						// Out U32
		case PREFIX_GET_CURRENT_BUF_SIZE	: return "Get current buffer size";
		case PREFIX_CLEAR_BUF_SPECTRA		: return "Clear all buffered spectra";
		case PREFIX_REMOVE_OLDEST_SPECTRA	: return "Remove oldest spectra";					// In U32
		case PREFIX_SET_CURRENT_BUF_SIZE	: return "Set current buffer size";					// In U32
		case PREFIX_GET_NB_SPECTRA			: return "Get current number of spectra in the buffer";	// Out U32
		case PREFIX_GET_RAW_SPECTRUM_META	: return "Get raw spectrum with metadata";			// In U32, out U32[], U16, U32.
		
		case PREFIX_GET_SPECTRUM			: return "Get and send default spectrum immediately";	// Out U16[]
		case PREFIX_GET_MAX_ADC_COUNT		: return "Get max ADC counts";						// Out U16
		case PREFIX_GET_INTG_TIME			: return "Get integration time (us)";				// Out U32
		case PREFIX_GET_MIN_INTG_TIME		: return "Get min integration time (us)";			// Out U32
		case PREFIX_GET_MAX_INTG_TIME		: return "Get max integration time (us)";			// Out U32
		case PREFIX_GET_INTG_TIME_STEP		: return "Get integration time step size (us)";		// Out U32
		case PREFIX_SET_INTG_TIME			: return "Set integration time (us)";				// In U32
		
		case PREFIX_GET_TRIGGER_MODE		: return "Get trigger mode";						// Out U8
		case PREFIX_GET_NB_SPECTRA_PER_TRIG	: return "Get number of back-to-back spectra per trigger event";	// Out U32
		case PREFIX_SET_TRIGGER_MODE		: return "Set trigger mode";						// In U8
		case PREFIX_SET_NB_SPECTRA_PER_TRIG	: return "Set number of back-to-back spectra per trigger event";	// In U32
		
		case PREFIX_BINNING					: return "Binning";									// Undocumented and unimplemented
		case PREFIX_GET_LAMP_ENABLE			: return "Get lamp enable";							// Out U8
		case PREFIX_SET_LAMP_ENABLE			: return "Set lamp enable";							// In U8
		
		case PREFIX_GET_ACQ_DELAY			: return "Get acquisition delay";					// Out U32
		case PREFIX_GET_MIN_ACQ_DELAY		: return "Get min acquisition delay";				// Out U32
		case PREFIX_GET_MAX_ACQ_DELAY		: return "Get max acquisition delay";				// Out U32
		case PREFIX_GET_ACQ_DELAY_STEP		: return "Get acquisition delay step size";			// Out U32
		case PREFIX_SET_ACQ_DELAY			: return "Set acquisition delay";					// In U32
		
		case PREFIX_GET_SCANS_TO_AVG		: return "Get scans to average";					// Out U16
		case PREFIX_SET_SCANS_TO_AVG		: return "Set scans to average";					// In U16
		case PREFIX_BOXCAR					: return "Boxcar";									// Undocumented and unimplemented
		
		case PREFIX_GET_WL_COEF_COUNT		: return "Get wavelength coefficient count";
		case PREFIX_GET_WL_COEF				: return "Get wavelength coefficient";
		case PREFIX_SET_WL_COEF				: return "Set wavelength coefficient";				// Undocumented
		case PREFIX_GET_NL_COEF_COUNT		: return "Get nonlinearity coefficient count";
		case PREFIX_GET_NL_COEF				: return "Get nonlinearity coefficient";
		case PREFIX_GET_SL_COEF_COUNT		: return "Get stray light coefficient count";
		case PREFIX_GET_SL_COEF				: return "Get stray light coefficient";
		
		case PREFIX_GPIO					: return "GPIO";									// None implemented below
		case PREFIX_SINGLE_STROBE			: return "Single strobe";
		case PREFIX_CONTINUOUS_STROBE		: return "Continuous strobe";

		case PREFIX_GET_NB_TEMPS			: return "Get number of temperature sensors";
		case PREFIX_GET_TEMP				: return "Read temperature sensor";
		case PREFIX_GET_ALL_TEMPS			: return "Read all temperature sensors";

		case PREFIX_SPI						: return "SPI";
		case PREFIX_I2C						: return "I2C";

		case PREFIX_GET_BATTERY_LEVEL		: return "Get battery charge level";
		case PREFIX_GET_BATTERY_STATUS		: return "Get battery (dis)charging status";
		case PREFIX_GET_BATTERY_TEMP		: return "Read battery temperature sensor";
		
		default								: return "UNKNOWN MESSAGE TYPE";
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Returns a string with the trigger mode explained
////////////////////////////////////////////////////////////////////////////////
static char* TriggerModeStr(int TriggerMode) {
	switch (TriggerMode) {
		case    0: return "Normal trigger mode";
		case    1: return "Hardware rising edge";
		case    3: return "Hardware level";
		case 0xFF: return "Disabled";
		default  : return "UNKNOWN TRIGGER MODE";
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Performs some sanity check on a packet (either before sending or after receiving)
/// HIRET	0 if no error, 1 if error in header, 2 if error in footer, 3 if error in payload size
////////////////////////////////////////////////////////////////////////////////
static int VerifyMessage(tOBPh* Header, tOBPf* Footer) {
	if (memcmp(Header->StartBytes, DefaultHeader.StartBytes, 2)) { printf(BLD RED "Invalid start bytes\n" NRM); return 1; }
	if (memcmp(Footer->Footer    , DefaultFooter.Footer    , 4)) { printf(BLD RED "Invalid footer\n" NRM); return 2; }
	if (Header->BytesRemaining<20) { printf(BLD RED "Invalid bytes remaining:%d\n" NRM, Header->BytesRemaining); return 3; }
	// TODO: checksum verification unimplemented
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Display information about a packet (either before sending of after receiving)
/// HIRET	0 if buffer format seems valid
////////////////////////////////////////////////////////////////////////////////
static int DisplayRawMessage(const unsigned char* Buffer, int Size) {
	int R=0;
	tOBPh* Header=(tOBPh*)Buffer;
	
	// Raw display
#ifdef USE_COLOR_MSG
	// Visually enhances the payload
	int Start= (Header->ImmediateDataLen ? 24 : 44);
	int End  = (Header->ImmediateDataLen ? 24+Header->ImmediateDataLen : 44+Header->BytesRemaining-sizeof(tOBPf))-1;
	if (Header->ImmediateDataLen==0 and Header->BytesRemaining==sizeof(tOBPf)) Start=End=-1;	// No data
	for (int i=0; i<Size; i++) printf(" %s%02X%s", i==Start?BLD:"", Buffer[i], i==End?NRM:""); 
#else
	for (int i=0; i<Size; i++) printf(" %02X", Buffer[i]); 
#endif
	putchar('\n');

	int Expect=sizeof(tOBPh)+Header->BytesRemaining;
	if (Size!=Expect) { printf(BLD RED "Invalid buffer size:%d, expecting %d\n" NRM, Size, Expect); return 1; }
	
	tOBPf* Footer=(tOBPf*)(Buffer+sizeof(tOBPh)+Header->BytesRemaining-sizeof(tOBPf));
	R=VerifyMessage(Header, Footer);
	if (R==1) return R;
	// Detailed display header
	printf("Protocol:%d, Flags:0x%X (%s), Error:%d, MsgType:0x%X (" BLD "%s" NRM "), Regarding:%d, ChecksumType:%d, ImmediateDataLen:%d, Remaining:%d",
			Header->ProcotolVersion, Header->Flags, DisplayFlagErr(Header), Header->ErrorNumber,
			Header->MessageType, MessageTypeStr(Header->MessageType), Header->Regarding,
		   Header->ChecksumType, Header->ImmediateDataLen, Header->BytesRemaining);
	if (Header->ImmediateDataLen>0) putchar(':');
	for (int i=0; i<Header->ImmediateDataLen; i++) printf(" %02X", Header->ImmediateData[i]);
//	printf("\n");
		
	int PayloadSize=Header->BytesRemaining-sizeof(tOBPf);
	if (Header->ImmediateDataLen!=0 and PayloadSize!=0) printf(BLD RED "Immediate data and payload are mutualy exclusive" NRM); 
	// Detailed display payload - somewhat redundant with raw display ?
	if (PayloadSize) printf(", PayloadLen:%d", PayloadSize);
	putchar('\n');
	
	if (R==2) return R;
	// Detailed display footer - nothing interesting here
	
	return R;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Prepares a data packet for sending. Puts data in appropriate area
/// HIPAR	Data/Data to be copied to immediate or payload area
/// HIPAR	DataLen/Size of the payload/immediate data
/// HIRET	Pointer to data packet (header+payload+footer), YOU MUST FREE THIS AFTER USE
///////////////////////////////////////////////////////////////////////////////
static unsigned char* MakePacket(int MessageType, unsigned char* Data, int DataLen) {
	if (DataLen<=16 and !NoImmediate) { // Use Immediate area, no payload
		unsigned char *Buf=malloc(sizeof(tOBPh)+sizeof(tOBPf));
		tOBPh *Header=(tOBPh*)Buf;					*Header=DefaultHeader;
		tOBPf *Footer=(tOBPf*)(Buf+sizeof(tOBPh));	*Footer=DefaultFooter;
		Header->MessageType=MessageType;
		if (Data!=NULL) memcpy(Header->ImmediateData, Data, Header->ImmediateDataLen=DataLen);
		Header->BytesRemaining=sizeof(tOBPf);
		return Buf;
	} else {
		unsigned char *Buf=malloc(sizeof(tOBPh)+DataLen+sizeof(tOBPf));
		tOBPh *Header=(tOBPh*)Buf;										*Header=DefaultHeader;
		tOBPf *Footer=(tOBPf*)(Buf+sizeof(tOBPh)+DataLen);				*Footer=DefaultFooter;
		Header->MessageType=MessageType;
		if (Data!=NULL) memcpy(Buf+sizeof(tOBPh), Data, DataLen);
		Header->BytesRemaining =   sizeof(tOBPf) +      DataLen;
		return Buf;
	}
	NoImmediate=0;
}

///////////////////////////////////////////////////////////////////////////////
// Those macros are shortcuts
// Packet must be a pointer to a well formed OBP packet
#define SizeOfPacket(Packet) (sizeof(tOBPh)+( ((tOBPh*)Packet)->BytesRemaining==0 ? sizeof(tOBPf) : ((tOBPh*)Packet)->BytesRemaining ))
// Return length of data packet, either immediaet or payload
#define SizeOfData(Packet)   ( ((tOBPh*)Packet)->ImmediateDataLen ? ((tOBPh*)Packet)->ImmediateDataLen : ((tOBPh*)Packet)->BytesRemaining-sizeof(tOBPf) )
// Points to either ImmediateData or payload
#define DataOfPacket(Packet) ( ((tOBPh*)Packet)->ImmediateDataLen ? ((tOBPh*)Packet)->ImmediateData    : (Packet+sizeof(tOBPh)) )
#define SIZE_CHECK(type) if (SizeOfData(Reply)!=sizeof(type)) { printf(BLD RED "Expecting size %ld (" #type "), got %ld\n" NRM, sizeof(type), SizeOfData(Reply)); return 0; }

///////////////////////////////////////////////////////////////////////////////
// Generic commands here:

/// HIFN	Send a simple command without parameters without reading a reply
static int OceanHDX_Simple(int MessageType) {
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	int R=SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return R;
}

/// HIRET	Return a 'Get U8' value. Applies to several types of messages
static unsigned char OceanHDX_GetU8(int MessageType) {
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	unsigned char Reply[1024];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	SIZE_CHECK(char);
	return *(unsigned char*)DataOfPacket(Reply);
}

/// HIRET	Return a 'Get U16' value. Applies to several types of messages
static unsigned short OceanHDX_GetU16(int MessageType) {
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	unsigned char Reply[1024];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	SIZE_CHECK(short);
	return *(unsigned short*)DataOfPacket(Reply);
}

/// HIRET	Return a 'Get U32' value. Applies to several types of messages
static unsigned int OceanHDX_GetU32(int MessageType) {
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	unsigned char Reply[1024];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	SIZE_CHECK(int);
	return *(unsigned int*)DataOfPacket(Reply);
}

/// HIRET	Return a 'Get U64' value. Applies to several types of messages
static unsigned long long OceanHDX_GetU64(int MessageType) {
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	unsigned char Reply[1024];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	SIZE_CHECK(long long);
	return *(unsigned long long*)DataOfPacket(Reply);
}

static float OceanHDX_GetFloat(int MessageType) {
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	unsigned char Reply[1024];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	SIZE_CHECK(float);
	return *(float*)DataOfPacket(Reply);
}


static float OceanHDX_SetU8_GetFloat(int MessageType, unsigned char U8) {
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&U8, sizeof(char));
	unsigned char Reply[1024];
	int Received=0;
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), U8);
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	SIZE_CHECK(float);
	return *(float*)DataOfPacket(Reply);
}


/// HIFN	Set a U8, does not read a reply
static int OceanHDX_SetU8(int MessageType, unsigned char Val) {
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Val, sizeof(char));
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), Val);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}

/// HIFN	Set a U16, does not read a reply
static int OceanHDX_SetU16(int MessageType, unsigned short Val) {
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Val, sizeof(short));
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), Val);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}

/// HIFN	Set a U32, does not read a reply
static int OceanHDX_SetU32(int MessageType, unsigned int Val) {
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Val, sizeof(int));
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), Val);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}

/// HIFN	Set a U64, does not read a reply
static int OceanHDX_SetU64(int MessageType, unsigned long long Val) {
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Val, sizeof(long long));
	printf(BLD YEL "%s: %lld\n" NRM, MessageTypeStr(MessageType), Val);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}

/// HIFN	Set a float, does not read a reply
__attribute__((unused)) 
static int OceanHDX_SetFloat(int MessageType, float Val) {
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Val, sizeof(float));
	printf(BLD YEL "%s: %f\n" NRM, MessageTypeStr(MessageType), Val);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}


/// HIRET	Return a string. Applies to several types of messages
/// HIRET	We assume the returned string is null-terminated but there's no guaranty
static char* OceanHDX_GetString(int MessageType) {
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	static unsigned char Reply[1024];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	//	printf(BLD GRN "String: %s\n" NRM, (char*)DataOfPacket(Reply));
	return (char*)DataOfPacket(Reply);
}

/// HIFN	Set a string, does not read a reply. Final \0 is included. Seems to work either way
static int OceanHDX_SetString(int MessageType, char* String) {
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)String, strlen(String)+1);
	printf(BLD YEL "%s: %s\n" NRM, MessageTypeStr(MessageType), String);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// Specific commands from now on:

// WARNING: this will reset the ethernet connection, don't expect a reply for a minute after
int OceanHDX_Reset(void) {
	return OceanHDX_Simple(PREFIX_RESET);
}

// WARNING: this will reset the ethernet connection and lose some configs, don't expect a reply for a minute after
int OceanHDX_ResetRestore(void) {
	return OceanHDX_Simple(PREFIX_RESET_RESTORE);
}

int OceanHDX_GetRecoveryStatus(void) {
	int MessageType=PREFIX_GET_RECOVERY_STATUS;
	int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

///////////////////////////////////////////////////////////////////////////////

/// HIFN	This is a compound command of 3 others commands
/// HIRET	Static string, do not free
char* OceanHDX_GetFirmware(void) {
	static char Firmware[256];
	int MessageType=PREFIX_GET_MAIN_FIRMWARE_REV;
	unsigned int M1=OceanHDX_GetU16(MessageType);
	
	MessageType=PREFIX_GET_MAIN_FIRMWARE_SUB;
	unsigned int M2=OceanHDX_GetU16(MessageType);
	
	MessageType=PREFIX_GET_SECOND_FIRMWARE_REV;
	unsigned int M3=OceanHDX_GetU16(MessageType);
	
	sprintf(Firmware, "IMX6:%x.%02x, FPGA:%x", M1, M2, M3);	// BCD info can be displayed directly with %x
	printf(BLD GRN "Firmware: %s\n" NRM, Firmware);
	return Firmware;
}

/// HIFN	Return the serial number
/// HIRET	Static string, do not free
char* OceanHDX_GetSerial(void) {
	char *Str=OceanHDX_GetString(PREFIX_GET_MAIN_SERIAL);
	printf(BLD GRN "Serial: %s\n" NRM, Str);
	return Str;
}

/// HIRET	Return the device alias
/// HIRET	Static string, do not free
char* OceanHDX_GetAlias(void) {
	char *Alias=OceanHDX_GetString(PREFIX_GET_DEV_ALIAS);
	printf(BLD GRN "Alias: %s\n" NRM, Alias);
	return Alias;
}

int OceanHDX_SetAlias(char *Alias) {
	return OceanHDX_SetString(PREFIX_SET_DEV_ALIAS, Alias);
}

int OceanHDX_GetUserStringCount(void) {
	int MessageType=PREFIX_GET_USER_STR_COUNT;
	unsigned int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetUserStringLen(int Idx) {
	int MessageType=PREFIX_GET_USER_STR_LEN;
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Idx, sizeof(char));
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), Idx);
	unsigned char Reply[1024];	// Doesn't need to be static
	int Received=0;
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	SIZE_CHECK(short);
	return *(unsigned short*)DataOfPacket(Reply);
}

char* OceanHDX_GetUserString(int Idx) {
	int MessageType=PREFIX_GET_USER_STR;
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Idx, sizeof(char));
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), Idx);
	static unsigned char Reply[1024];
	int Received=0;
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	return (char*)DataOfPacket(Reply);
}

int OceanHDX_SetUserString(int Idx, char* Str) {
	int MessageType=PREFIX_SET_USER_STR;
	char Transfer[256];
	*(unsigned char*)Transfer=(unsigned char)Idx;
	strcpy(Transfer+1, Str);
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)Transfer, strlen(Str)+2);
	printf(BLD YEL "%s: %d %s\n" NRM, MessageTypeStr(MessageType), Idx, Str);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}

/// HIFN	Undocumented and does not seem to work
/// HIPAR	Status/0:normal, 1:SOS, 2:Fade in and out
int OceanHDX_SetStatusLED(unsigned char Status) {
	unsigned char Send[2]={0, Status};
	return OceanHDX_SetU16(PREFIX_SET_STATUS_LED, *(unsigned short*)Send);
}


/// HIRET	An informative string - You can call this in Ethernet mode
char* OceanHDX_GetUsbInfo(void) {
	static char Info[256];
	int VID=OceanHDX_GetU16(0xE00);
	int PID=OceanHDX_GetU16(0xE01);
	char Manufacturer[80]; strcpy(Manufacturer, OceanHDX_GetString(0xE10));
	char Device[80]; strcpy(Device, OceanHDX_GetString(0xE11));
	sprintf(Info, "VID:%04X, PID:%04X, %s, %s", VID, PID, Manufacturer, Device);
	printf(BLD GRN "USB info: %s\n" NRM, Info);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get current onboard time since Jan 1 1970 in us
time_t OceanHDX_GetCurrentTime(void) {
	int MessageType=PREFIX_GET_TIME;
	unsigned long long Res=OceanHDX_GetU64(MessageType);
	printf(BLD GRN "%s: %lld\n" NRM, MessageTypeStr(MessageType), Res);
	return Res/1000000;
}

/// HIFN	Set current onboard time since Jan 1 1970 in us
int OceanHDX_SetCurrentTime(void) {
	time_t Time=time(NULL);
	// Optional display
	char Str[256];
	struct tm TM;
	localtime_r(&Time, &TM);
	strftime(Str, 256, "%Y%m%d-%H%M%S", &TM);
	printf("Setting time: %s\n", Str);
	
	// We apparently have to do the call twice, otherwise it doesn't work. Firmware bug ?
	       OceanHDX_SetU64(PREFIX_SET_TIME, Time*1000000); sleep(0);
	return OceanHDX_SetU64(PREFIX_SET_TIME, Time*1000000);
}

///////////////////////////////////////////////////////////////////////////////

/// HIPAR	Len/Size of requested payload. Note: values <=16 and not multiple of 4 (Firmware bug ???) seem to fail
/// HIRET	Returns the size of the obtained payload (should match Len)
int OceanHDX_GetTestPayload(unsigned int Len) {
	int MessageType=PREFIX_GET_PAYLOAD;
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&Len, sizeof(int));
	unsigned char Reply[sizeof(tOBPh)+Len+sizeof(tOBPf)];
	int Received=0;
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), Len);
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	if (SizeOfData(Reply)==Len) printf(BLD GRN "PASSED\n" NRM);
	else                        printf(BLD RED "FAILED\n" NRM);
	return SizeOfData(Reply);
}

/// HIFN	Send a payload and ask for an echo
/// HIRET	Size of received payload (should match)
int OceanHDX_EchoPayload(unsigned char* Payload, unsigned int Len) {
	int MessageType=PREFIX_ECHO_PAYLOAD;
	unsigned char* Command=MakePacket(MessageType, Payload, Len);
	unsigned char Reply[sizeof(tOBPh)+Len+sizeof(tOBPf)];
	int Received=0;
	printf(BLD YEL "%s: %d\n" NRM, MessageTypeStr(MessageType), Len);
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	if (SizeOfData(Reply)==Len) printf(BLD GRN "PASSED\n" NRM);
	else                        printf(BLD RED "FAILED\n" NRM);
	return SizeOfData(Reply);
}

int OceanHDX_Abort(void) {
	return OceanHDX_Simple(PREFIX_ABORT);
		
}

///////////////////////////////////////////////////////////////////////////////
int OceanHDX_GetBufEnabled(void) {
	int MessageType=PREFIX_GET_BUF_ENABLED;
	unsigned int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetMaxBufSize(void) {
	int MessageType=PREFIX_GET_MAX_BUF_SIZE;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetCurrentBufSize(void) {
	int MessageType=PREFIX_GET_CURRENT_BUF_SIZE;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_ClearBuffer(void) {
	return OceanHDX_Simple(PREFIX_CLEAR_BUF_SPECTRA);
}

int OceanHDX_RemoveOldestSpectra(unsigned int Nb) {
	return OceanHDX_SetU32(PREFIX_REMOVE_OLDEST_SPECTRA, Nb);
}

int OceanHDX_SetCurrentBufSize(unsigned int Size) {
	return OceanHDX_SetU32(PREFIX_REMOVE_OLDEST_SPECTRA, Size);
}

int OceanHDX_GetNbSpectra(void) {
	int MessageType=PREFIX_GET_NB_SPECTRA;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

///////////////////////////////////////////////////////////////////////////////

/// HIRET	Number of scans to average
int OceanHDX_GetScansToAverage(void) {
	int MessageType=PREFIX_GET_SCANS_TO_AVG;
	unsigned int Res=OceanHDX_GetU16(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_SetScansToAverage(unsigned short Avg) {
	return OceanHDX_SetU32(PREFIX_SET_SCANS_TO_AVG, Avg);
}

int OceanHDX_GetMaxAdcCounts(void) {
	int MessageType=PREFIX_GET_MAX_ADC_COUNT;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

///////////////////////////////////////////////////////////////////////////////

/// HIPAR	Intg/In us
int OceanHDX_GetIntegrationTime(void) {
	int MessageType=PREFIX_GET_INTG_TIME;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_SetIntegrationTime(unsigned int Intg) {
	return OceanHDX_SetU32(PREFIX_SET_INTG_TIME, Intg);
}

int OceanHDX_GetMinIntegrationTime(void) {
	int MessageType=PREFIX_GET_MIN_INTG_TIME;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetMaxIntegrationTime(void) {
	int MessageType=PREFIX_GET_MAX_INTG_TIME;
	unsigned int  Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetIntegrationTimeStepSize(void) {
	int MessageType=PREFIX_GET_INTG_TIME_STEP;
	unsigned int  Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

///////////////////////////////////////////////////////////////////////////////
int OceanHDX_GetTriggerMode(void) {
	int MessageType=PREFIX_GET_TRIGGER_MODE;
	unsigned int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d (%s)\n" NRM, MessageTypeStr(MessageType), Res, TriggerModeStr(Res));
	return Res;
}

int OceanHDX_SetTriggerMode(int TriggerMode) {
	return OceanHDX_SetU8(PREFIX_SET_TRIGGER_MODE, TriggerMode);
}

int OceanHDX_GetNbSpectraPerTrig(void) {
	int MessageType=PREFIX_GET_NB_SPECTRA_PER_TRIG;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_SetNbSpectraPerTrig(unsigned int Nb) {
	return OceanHDX_SetU32(PREFIX_GET_NB_SPECTRA_PER_TRIG, Nb);
}

///////////////////////////////////////////////////////////////////////////////

int OceanHDX_GetLampEnable(void) {
	int MessageType=PREFIX_GET_LAMP_ENABLE;
	unsigned int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_SetLampEnable(int TriggerMode) {
	return OceanHDX_SetU8(PREFIX_SET_LAMP_ENABLE, TriggerMode);
}

///////////////////////////////////////////////////////////////////////////////

/// HIPAR	Intg/In us
int OceanHDX_GetAcqDelay(void) {
	int MessageType=PREFIX_GET_ACQ_DELAY;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_SetAcqDelay(unsigned int Intg) {
	return OceanHDX_SetU32(PREFIX_SET_ACQ_DELAY, Intg);
}

int OceanHDX_GetMinAcqDelay(void) {
	int MessageType=PREFIX_GET_MIN_ACQ_DELAY;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetMaxAcqDelay(void) {
	int MessageType=PREFIX_GET_MAX_ACQ_DELAY;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetAcqDelayStepSize(void) {
	int MessageType=PREFIX_GET_ACQ_DELAY_STEP;
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %dus\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

///////////////////////////////////////////////////////////////////////////////

// NOTE: The Microcontroller Sensor Temperature runs at a higher temperature than the Detector Board Thermistor

int OceanHDX_GetNbOfTemperatures(void) {
	int MessageType=PREFIX_GET_NB_TEMPS;
//	unsigned int Res=OceanHDX_GetU8(MessageType);	// According to the specs. Firmware error ???
	unsigned int Res=OceanHDX_GetU32(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

float OceanHDX_GetTemperature(int Index) {
	int MessageType=PREFIX_GET_TEMP;
	float Res=OceanHDX_SetU8_GetFloat(MessageType, Index);
	printf(BLD GRN "%s %d: %.1fdegC\n" NRM, MessageTypeStr(MessageType), Index, Res);
	return Res;
}
	
float* OceanHDX_GetAllTemperatures(void) {
	int MessageType=PREFIX_GET_ALL_TEMPS;
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	static unsigned char Reply[256*sizeof(float)];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);
	return (float*)DataOfPacket(Reply);
}

///////////////////////////////////////////////////////////////////////////////
// NOTE: Those functions don't seem to work, so they are not exported in the .h
//       Maybe try them on different models, who knows...

float OceanHDX_GetBatteryLevel(void) {
	int MessageType=PREFIX_GET_BATTERY_LEVEL;
	float Res=OceanHDX_GetFloat(MessageType);
	printf(BLD GRN "%s: %.1f%%\n" NRM, MessageTypeStr(MessageType), Res*100);
	return Res;
}

char* BatteryStatus[]={"Unknown", "Charging", "Discharging", "Charged"};
int OceanHDX_GetBatteryStatus(void) {
	int MessageType=PREFIX_GET_BATTERY_STATUS;
	int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %s\n" NRM, MessageTypeStr(MessageType), BatteryStatus[Res]);
	return Res;
}

float OceanHDX_GetBatteryTemp(void) {
	int MessageType=PREFIX_GET_BATTERY_TEMP;
	float Res=OceanHDX_GetFloat(MessageType);
	printf(BLD GRN "%s: %.1fdeg\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}


///////////////////////////////////////////////////////////////////////////////

int OceanHDX_GetWavelengthCoefCount(void) {
	int MessageType=PREFIX_GET_WL_COEF_COUNT;
	unsigned int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetNonlinearityCoefCount(void) {
	int MessageType=PREFIX_GET_NL_COEF_COUNT;
	unsigned int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

int OceanHDX_GetStrayLightCoefCount(void) {
	int MessageType=PREFIX_GET_SL_COEF_COUNT;
	unsigned int Res=OceanHDX_GetU8(MessageType);
	printf(BLD GRN "%s: %d\n" NRM, MessageTypeStr(MessageType), Res);
	return Res;
}

float OceanHDX_GetWavelengthCoef(unsigned char Index) {
	return OceanHDX_SetU8_GetFloat(PREFIX_GET_WL_COEF, Index);
}

float OceanHDX_GetNonlinearityCoef(unsigned char Index) {
	return OceanHDX_SetU8_GetFloat(PREFIX_GET_NL_COEF, Index);
}

/// HIFN	This returns no data (instead of a U8) even if the coef number is >0. Firmware bug ???
float OceanHDX_GetStrayLightCoef(unsigned char Index) {
	return OceanHDX_SetU8_GetFloat(PREFIX_GET_SL_COEF, Index);
}

/// HIFN	Dangerous ! This will replace the values in the firmware. Make sure you keep the original values
int OceanHDX_SetWavelengthCoef(unsigned char Index, float Coef) {
	typedef struct sU8F { unsigned char I; float C; } __attribute__((packed)) tU8F;
	assert(sizeof(tU8F)==5);
	tU8F U8F={Index, Coef};
	int MessageType=PREFIX_SET_WL_COEF;
	unsigned char* Command=MakePacket(MessageType, (unsigned char*)&U8F, sizeof(tU8F));
	printf(BLD YEL "%s: %d %g\n" NRM, MessageTypeStr(MessageType), U8F.I, U8F.C);
	SendCommand(Command, SizeOfPacket(Command), NULL, NULL);
	free(Command);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
tOceanHDX_Coefs OceanHDX_Wavelength={0}, OceanHDX_Nonlinearity={0}, OceanHDX_StrayLight={0};

double OceanHDX_Wavelengths[PIXEL_NB]={0};	// Array of wavelengths for each pixel. 
// Computed once by Lambda() below as soon as we have read the wavelength coefficients in OceanHDX_GetCoefs()

/// HIFN	Compute the wavelengths polynomial and store in global array OceanHDX_Wavelengths[]
/// HIFN	See https://oceanoptics.com/wp-content/uploads/Spectrometer-Wavelength-Calibration-Instructions.pdf
static void Lambda(void) {
	for (int p=0; p<PIXEL_NB; p++) {
		OceanHDX_Wavelengths[p]=OceanHDX_Wavelength.Coefs[0];
		double PP=p;
		for (int i=1; i<OceanHDX_Wavelength.NB; i++, PP*=p)
			OceanHDX_Wavelengths[p]+=OceanHDX_Wavelength.Coefs[i]*PP; 
	}
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get all coefficients. Arrays must be pre-allocated and big enough (up to 256)
/// HIFN	After calling this, you can use OceanHDX_Wavelengths[PIXEL_NB] to get the wavelengths of each pixel
///////////////////////////////////////////////////////////////////////////////
int OceanHDX_GetCoefs(void) {
	OceanHDX_Wavelength.NB=OceanHDX_GetWavelengthCoefCount();
	for (int i=0; i<OceanHDX_Wavelength.NB; i++) OceanHDX_Wavelength.Coefs[i]=OceanHDX_GetWavelengthCoef(i);
	printf(BLD GRN "%d wavelengths coefficients:", OceanHDX_Wavelength.NB);
	for (int i=0; i<OceanHDX_Wavelength.NB; i++) printf(" %g", OceanHDX_Wavelength.Coefs[i]);
	printf("\n" NRM);
	
	OceanHDX_Nonlinearity.NB=OceanHDX_GetNonlinearityCoefCount();
	for (int i=0; i<OceanHDX_Nonlinearity.NB; i++) OceanHDX_Nonlinearity.Coefs[i]=OceanHDX_GetNonlinearityCoef(i);
	printf(BLD GRN "%d nonlinearity coefficients:", OceanHDX_Nonlinearity.NB);
	for (int i=0; i<OceanHDX_Nonlinearity.NB; i++) printf(" %g", OceanHDX_Nonlinearity.Coefs[i]);
	printf("\n" NRM);
	
	OceanHDX_StrayLight.NB=OceanHDX_GetStrayLightCoefCount();
	for (int i=0; i<OceanHDX_StrayLight.NB; i++) OceanHDX_StrayLight.Coefs[i]=OceanHDX_GetStrayLightCoef(i);
	printf(BLD GRN "%d stray light coefficients:", OceanHDX_StrayLight.NB);
	for (int i=0; i<OceanHDX_StrayLight.NB; i++) printf(" %g", OceanHDX_StrayLight.Coefs[i]);
	printf("\n" NRM);
	
	Lambda();	// Precompute the wavelength array
	return 0;
}

/// HIFN	DANGEROUS ! as it will set the values inside the firmware
int OceanHDX_SetWavelengthCoefs(float I, float C1, float C2, float C3) {
	if (OceanHDX_Wavelength.NB!=4) { printf(BLD RED "Trying to set 4 coefs, but %d found\n" NRM, OceanHDX_Wavelength.NB); return 1; }
	printf(BLD GRN "Setting wavelengths coefficients: I=%g C1=%g C2=%g C3=%g\n", I, C1, C2, C3);
	return	OceanHDX_SetWavelengthCoef(0, I) or
			OceanHDX_SetWavelengthCoef(1, C1) or
			OceanHDX_SetWavelengthCoef(2, C2) or
			OceanHDX_SetWavelengthCoef(3, C3);
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Obtain an immediate spectrum
/// HIFN	The pixel intensities are corrected for temperature drift and fixed-pattern noise.
/// OUT		Size
/// HIPAR	Size/Number of U16 samples taken
/// HIRET	Array of pixel intensities of size values. Do not free this array (it's in static memory)
///////////////////////////////////////////////////////////////////////////////
tSpectroPixels* OceanHDX_GetImmediateSpectrum(int *Size) {
	int MessageType=PREFIX_GET_SPECTRUM;
	unsigned char* Command=MakePacket(MessageType, NULL, 0);
	static unsigned char Reply[MAX_REPLY];	// Maybe instead do read header, allocate, then read remains instead of allocating large static buffer. But memory is cheap, who cares...
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);

	*Size=SizeOfData(Reply)/2;
	return (tSpectroPixels*)DataOfPacket(Reply);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Obtain a raw spectrum with metadata
/// HIPAR	Number of spectra to read. Default 0==1
/// HIRET	Nb arrays of size SIZE_OF_RAW in succession. Do not free this array (it's in static memory)
///////////////////////////////////////////////////////////////////////////////
tSpectroPixels* OceanHDX_GetRawSpectrumWithMetaData(int Nb, int *Size) {
	int MessageType=PREFIX_GET_RAW_SPECTRUM_META;
	unsigned char* Command=Nb>0 ? MakePacket(MessageType, (unsigned char*)&Nb, sizeof(int)) : MakePacket(MessageType, NULL, 0);
	static unsigned char Reply[MAX_REPLY];
	int Received=0;
	printf(BLD YEL "%s\n" NRM, MessageTypeStr(MessageType));
	SendCommand(Command, SizeOfPacket(Command), Reply, &Received);
	free(Command);

	tMetadata *Meta=(tMetadata*)DataOfPacket(Reply);

	time_t T=Meta->TimeUS/1000000;
	struct tm TM;  localtime_r(&T, &TM);
	char Str[256]; strftime(Str, 256, "%Y%m%d-%H%M%S", &TM);
	//printf("Onboard device time: %s\n", Str);

	printf(BLD GRN "Metadata: protocol:%d, "
			"Len:%d, PixelDatalen:%d, "
			/*"Counter:%X%08Xus, "*/ "Time:%s, IntegrationTime:%dus, "
			"PixelDataFormat:%s, "
			"Count:%d, LastCount:%d, "
			"LastCounterL:%X, LastCounterM:%X, ScansToAverage:%d\n" NRM,
			Meta->Protocol, 
			Meta->Length, Meta->TotalPixelDataLength,
			/*Meta->CounterMSW, Meta->CounterLSW,*/ Str, Meta->IntegrationTime,
			Meta->PixelDataFormat==1?"U16":Meta->PixelDataFormat==2?"U24":Meta->PixelDataFormat==3?"U32":Meta->PixelDataFormat==4?"SPFP":"UNKNOWN", 
			Meta->SpectrumCount, Meta->LastSpectrumCount,
			Meta->LastCounterLSW, Meta->LastCounterMSW, Meta->ScansToAverage
	);

	*Size=Meta->TotalPixelDataLength/2;
	return (tSpectroPixels*)((char*)Meta+sizeof(tMetadata));
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Correct the acquired array for non-linearities
/// HIFN	I could not find specific info for the HDX, so I used the "Spark User Guide.pdf"
/// HIPAR	S/Acquired data, containing Spectrum array
/// HIPAR	Nb/Number of samples, normally PIXEL_NB
/// HIPAR	Corrected/Array corrected for non-linearities
/// HIPAR	Max/Max value found in the original array. If 0xFFFF, then you have saturated the spectrometer
/// OUT		Corrected, Max
/// HIRET	Return number of saturated points
///////////////////////////////////////////////////////////////////////////////
int OceanHDX_CorrectForNonLinearities(tSpectroPixels* S, int Nb, double Corrected[], unsigned short *Max) {
	/*	double AvgDark=( (double)		// FIXME: this if for the electronic dark removal, but I couldn't find the exact method to use
			((tSpectroPixels*)S)->Dark [0] +
			((tSpectroPixels*)S)->Dark [1] + 
			((tSpectroPixels*)S)->Dark [2] + 
			((tSpectroPixels*)S)->Dark2[0] + 
			((tSpectroPixels*)S)->Dark2[1] + 
			((tSpectroPixels*)S)->Dark2[2] + 
			((tSpectroPixels*)S)->Dark2[3]
	)/7.;	// Don't know what to do with this info. My first guess was to substrat the average dark, but it doesn't work
	 */	
	unsigned short Min=USHRT_MAX, NbSaturation=0;
	*Max=0;
	for (int j=0; j<Nb; j++) {
		double V=S->Spectrum[j];
		if ( Min>V)  Min=V;
		if (*Max<V) *Max=V;
//		if (GlobalMax<V) GlobalMax=V;
		if (0xFFFF==V) NbSaturation++; 
	}
	// NOTE: it kind of makes sense to also correct the dark/bevel values for nonlinearities. Not done here
	for (int j=0; j<PIXEL_NB; j++) {
		double x=S->Spectrum[j]-Min;	// Substract baseline. Maybe use AvgDark instead ???
										// Mathematically I don't see how that works
		// There's a theoretical 16 coef max, but the published formulas have either 4 or 7
		// which makes no difference if the last ones are 0, so no need to loop
		// We could use a lookup table: it's only half a Mb
		double F=  OceanHDX_Nonlinearity.Coefs[0]+
				x*(OceanHDX_Nonlinearity.Coefs[1]+
				x*(OceanHDX_Nonlinearity.Coefs[2]+
				x*(OceanHDX_Nonlinearity.Coefs[3]+
				x*(OceanHDX_Nonlinearity.Coefs[4]+
				x*(OceanHDX_Nonlinearity.Coefs[5]+
				x*(OceanHDX_Nonlinearity.Coefs[6]+
				x* OceanHDX_Nonlinearity.Coefs[7]))))));
		double R=x/F;
		Corrected[j]=R+Min;
	}
	return NbSaturation;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Substract the dark values from Spectro.
/// HIFN	First take a dark with the same integration value and keep it.
/// HIFN	Then take one or more normal spectrograms. Call this. Then correct for non linearities. Then save.
/// HIFN	Make sure the spectro does not saturate when taking Spectro
///////////////////////////////////////////////////////////////////////////////
void OceanHDX_DarkRemoval(tSpectroPixels* Spectro, tSpectroPixels *Dark) {
	unsigned short *S=(unsigned short *)Spectro, *D=(unsigned short *)Dark;
	for (int i=0; i<SIZE_OF_DATA/2; i++)
		if (S[i]>D[i]) S[i]-=D[i]; 
		else           S[i] =0;	// Underflows are possible
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Average several scans obtained by OceanHDX_GetRawSpectrumWithMetaData into the 1st one
/// HIFN	It would be better to do the nonlinearity correction and then the averagin -> more precision
/// HIPAR	P/Position of the 1st array as retrieved. This will contain the average (rounded since they are integers)
/// HIPAR	Nb/When using GetRawSpectrum, number of spectra retrieved (2..15).
/// HIRET	Number of saturated pixels
////////////////////////////////////////////////////////////////////////////////
int OceanHDX_BoxcarAvg(tSpectroPixels* P, int Nb) {
	int i, NbSaturation=0;
	
	if (Nb<2 or Nb>15) return 0;	// It's all 1-based, 0 is the same as 1 and there's 15 max
	void* S;
	
	for (int j=0; j<SIZE_OF_DATA/2; j++) {	// Include the dark and bevel in the averaging
		int Sum=0;
		for (i=1, S=P; i<=Nb; i++, S+=SIZE_OF_RAW) {
			Sum+=((unsigned short*)S)[j];
			if ( ((unsigned short*)S)[j]==0xFFFF) NbSaturation++;
		}
		((unsigned short*)P)[j]=Sum/Nb;
	}
	return NbSaturation;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Save the array to a file
/// HIFN	See: https://oceanoptics.com/faq/order-operations-spectral-calculations/
/// HIPAR	TimeStamp/Add date_time to filename. If 0 make sure to use differing TagNames if saving multiple files, otherwise you'll get overwrites
/// HIPAR	TagName/Something to add to the file name
/// HIPAR	Size/Should match SIZE_OF_DATA
/// HIPAR	Nb/When using GetRawSpectrum, number of spectra retrieved (1..15). 0 is the same as 1
/// HIPAR	Intg/Current integration value (in us)
////////////////////////////////////////////////////////////////////////////////
int OceanHDX_SaveArray(int DateTime, char* TagName, tSpectroPixels* P, int Size, int Nb, int Intg) {
	int NbSaturation=0, GlobalMax=INT_MIN;
	unsigned short LocalMax;
	char Date[256], Pathname[1024];
	time_t CurrentTime=time (NULL);
	struct tm TM2;
	strftime(Date, 255, "_%Y%m%d_%H%M%S", localtime_r(&CurrentTime, &TM2));
	sprintf(Pathname, "Spectrum%s%s.csv", DateTime?Date:"", TagName);
	FILE *fd=fopen(Pathname, "w");
	if (fd==NULL)   { printf(BLD RED "Could not write data to file %s: %s\n" NRM, Pathname, strerror(errno)); return 1; }
	if (Size!=2068) { printf(BLD RED "Data in invalid format, expecting 2068, found %d\n" NRM, Size); fclose(fd); return 2; }
	printf("Saving pixels to file %s\n", Pathname);
	
	if (Nb==0) Nb=1;	// It's all 1-based, 0 is the same as 1 and there's 15 max
	int i;
	void* S;
	double Corrected[16][PIXEL_NB]={0};		// Apply Non-linearity correction
	
	for (i=1, S=P; i<=Nb; i++, S+=SIZE_OF_RAW) {
		NbSaturation+=OceanHDX_CorrectForNonLinearities((tSpectroPixels*)S, PIXEL_NB, Corrected[i], &LocalMax);
		if (GlobalMax<LocalMax) GlobalMax=LocalMax;
	}
	// Will loop through multiple buffers returned by GetRawSpectrumWithMetadata
	#define FOR_EACH(What)   for (i=1, S=P; i<=Nb; i++, S+=SIZE_OF_RAW) fprintf(fd, "\t%d",   ((tSpectroPixels*)S)->What);
	
	fprintf(fd, "Wavelength (nm)\tIntensit%s\tCorrected\n", Nb<=1?"y":"ies");
	fprintf(fd, "Unusable"); FOR_EACH(Unusable); 
	fprintf(fd, "\nDark");   FOR_EACH(Dark[0]);
	fprintf(fd, "\nDark");   FOR_EACH(Dark[1]);
	fprintf(fd, "\nDark");   FOR_EACH(Dark[2]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel[0]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel[1]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel[2]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel[3]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel[4]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel[5]);
	for (int j=0; j<PIXEL_NB; j++) {
		fprintf(fd, "\n%.2f", OceanHDX_Wavelengths[j]); 
		FOR_EACH(Spectrum[j]);
				
		for (i=1, S=P; i<=Nb; i++, S+=SIZE_OF_RAW) 
			fprintf(fd, "\t%.1f", Corrected[i][j]);
	}
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel2[0]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel2[1]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel2[2]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel2[3]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel2[4]);
	fprintf(fd, "\nBevel");  FOR_EACH(Bevel2[5]);
	fprintf(fd, "\nDark");   FOR_EACH(Dark2[0]);
	fprintf(fd, "\nDark");   FOR_EACH(Dark2[1]);
	fprintf(fd, "\nDark");   FOR_EACH(Dark2[2]);
	fprintf(fd, "\nDark");   FOR_EACH(Dark2[3]);
	fprintf(fd, "\n");
	fclose(fd);
	if (NbSaturation>31) 
		printf(BLD RED "WARNING: CCD saturation on %.0f%% of pixels. Reduce integration time for better results.\n" NRM,
			   NbSaturation*100./(PIXEL_NB*Nb));
	else if (NbSaturation>0) 
			printf(BLD RED "WARNING: CCD saturation on %d pixels. Reduce integration time for better results.\n" NRM,
				   NbSaturation);
	else if (GlobalMax<0x6000) 
		printf(BLD YEL "Max value is %d, you should be able to raise the integration from %dms to ~%.0fms\n" NRM, 
			   GlobalMax, Intg/1000, Intg*65535./GlobalMax/1000);
	else 
		printf(BLD GRN "Optimum range: max is %d out of 65535\n" NRM, GlobalMax);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
#ifdef STANDALONE_TEST_PROG

static char IP[256]="192.168.0.4";	// Set by DHCP
static int Port=57357;

static const int VID=0x2457, PID=0x2003;	// Use lsusb to get this info

int main(int argc, const char* argv[]) {
	int Ret=0;
	errno=0;

	if (argc>1 and (0==strcmp("-h", argv[1]) or 0==strcmp("--help", argv[1]))) {
		fprintf(stderr, "%s [-USB] [addr [port]]: test the OceanHDX spectrometer via ethernet\n"
			"\t-USB\tUse USB device %04X:%04X instead of ethernet\n"\
			"\taddr:\tIP address of the spectrometer (default %s)\n"
			"\tport:\tport number (default %d)\n"
			, argv[0], VID, PID, IP, Port);
		return 1;
	}
	if (argc>1 and 0==strcmp(argv[1], "-USB")) {
		if ((Ret=OceanHDX_InitUSB(VID, PID))) return Ret;
	} else {
		if (argc>1) strcpy(IP, argv[1]);
		if (argc>2) Port=atoi(argv[2]);
		if ((Ret=OceanHDX_InitEth(IP, Port))) return Ret;
	}
	if (errno) { printf(BLD RED "Init error: %s\n" NRM, strerror(errno)); return errno; }
	
	// WARNING: those calls will reset the connection and restart DHCP,
	// don't expect a reply for a minute after calling them
	// OceanHDX_Reset();
	// OceanHDX_ResetRestore();
	
	char* Firmware=OceanHDX_GetFirmware();
	if (errno) { printf(BLD RED "ERROR: %s\n" NRM, strerror(errno)); return errno; }
	char* Serial  =OceanHDX_GetSerial();
	char* Alias   =OceanHDX_GetAlias();
	char* UsbInfo =OceanHDX_GetUsbInfo();
	
	if (OceanHDX_GetRecoveryStatus()) goto Skip;	// Better stop operation from here. Risky otherwise

	if (0<OceanHDX_GetUserStringCount())			// We should loop on all of them, but this is just a test
		if (0<OceanHDX_GetUserStringLen(0))
			OceanHDX_GetUserString(0);
	
	OceanHDX_SetCurrentTime();
	time_t Time =  OceanHDX_GetCurrentTime();	// Verification
	struct tm TM;  localtime_r(&Time, &TM);
	char Str[256]; strftime(Str, 256, "%Y%m%d-%H%M%S", &TM);
	printf("Onboard device time: %s\n", Str);
	
	// Undocumented and does not seem to work
//	OceanHDX_SetStatusLED(1); printf("Press [Enter]"); getchar();
//	OceanHDX_SetStatusLED(2); printf("Press [Enter]"); getchar();
//	OceanHDX_SetStatusLED(0);

	int NbTemps=OceanHDX_GetNbOfTemperatures();
	for (int i=0; i<NbTemps; i++) OceanHDX_GetTemperature(i);
	OceanHDX_GetAllTemperatures();	// This works also, same as above

//	OceanHDX_GetBatteryLevel();		// Doesn't seem to work
//	OceanHDX_GetBatteryStatus();
//	OceanHDX_GetBatteryTemp();
	
	// Payload tests
	OceanHDX_GetTestPayload(10);	// Some small or unaligned values fail, like this one. Bug in firmware ?
	if ( 20!=OceanHDX_GetTestPayload( 20)) goto Skip;
	if (300!=OceanHDX_GetTestPayload(300)) goto Skip;
	
	OceanHDX_EchoPayload((unsigned char*)"TEST", 5);					// Uses immediate memory
	OceanHDX_EchoPayload((unsigned char*)"This is a longer test", 22);	// uses payload memory
	
	unsigned int Avg, Intg, MinIntg, MaxIntg, StepIntg;
	// Various statuses
	Avg     =OceanHDX_GetScansToAverage();	// 1 by default
	Intg    =OceanHDX_GetIntegrationTime();	// Strangely This value can be lower than MinIntg... Firmware bug ?
	MinIntg =OceanHDX_GetMinIntegrationTime(); if (Intg<MinIntg) Intg=MinIntg;
	MaxIntg =OceanHDX_GetMaxIntegrationTime(); if (Intg<MaxIntg) Intg=MaxIntg;
	StepIntg=OceanHDX_GetIntegrationTimeStepSize();
	// Let's read many other optional parameters
	OceanHDX_GetBufEnabled();
	OceanHDX_GetMaxBufSize();	// The actual buffer can actually be bigger than that. Firmware bug ?
	OceanHDX_GetCurrentBufSize();
	OceanHDX_GetNbSpectra();
	OceanHDX_GetTriggerMode();
	OceanHDX_GetNbSpectraPerTrig();
	OceanHDX_GetLampEnable();
	OceanHDX_GetAcqDelay();
	OceanHDX_GetMinAcqDelay();
	OceanHDX_GetMaxAcqDelay();
	OceanHDX_GetAcqDelayStepSize();
	OceanHDX_GetMaxAdcCounts();
	
	// Now onto the important stuff
	OceanHDX_GetCoefs();	// The stray light fails. No big deal. Firmware bug ?
	
#if 0
	// This is risky as it manipulates the calibration of the spectrometer and is UNDOCUMENTED on the HDX
	// You MUST first have a backup of the original value first and a way to write it back properly
	// I HAVE TESTED IT AND IT WORKS. SEE OceanHDX_Calib.c FOR AUTOMATED CALIBRATION PROCEDURE
	if (OceanHDX_Wavelength.NB!=4) { printf(BLD RED "Unexpected number of calibration parameters: %d\n" NRM, OceanHDX_Wavelength.NB); goto Skip; }
	float Save=OceanHDX_Wavelength.Coefs[3], New=-1, Rst=-1;	// 344.229 0.362076 -4.50373e-05 3.10282e-09
	// Try to set a dummy coefficient to 0
	printf(BLD WHT "Current coef value: %g\n" NRM, Save);
	OceanHDX_SetWavelengthCoef(3, 0.);
	printf(BLD WHT "Changed coef value: %g\n" NRM, New=OceanHDX_GetWavelengthCoef(3));
	OceanHDX_SetWavelengthCoef(3, Save);
	printf(BLD WHT "Reset coef value: %g\n" NRM, Rst=OceanHDX_GetWavelengthCoef(3));
	if (!(New==0 and Rst==Save)) { printf(BLD RED "Set Coef Wavelength failure\n" NRM); goto Skip; }
	printf(BLD GRN "Set Coef Wavelength success !\n" NRM); 
#endif
	
#if 0	// Test some undocumented message types - None of those functions seem present in the HDX
	OceanHDX_GetString(PREFIX_GET_HOT_PIXELS);
	OceanHDX_GetString(PREFIX_GET_BENCH_ID);
	OceanHDX_GetString(PREFIX_GET_BENCH_SN);
	OceanHDX_GetU16(PREFIX_GET_SLIT_WIDTH);
	OceanHDX_GetString(PREFIX_GET_GRATING);
	OceanHDX_GetString(PREFIX_GET_FILTER);
	OceanHDX_GetString(PREFIX_GET_COATING);
#endif
	
	OceanHDX_ClearBuffer();
	
	printf("\nPress [Enter] to continue: "); getchar();
	char Thunk[80];
	#define DISP printf(BLD PRP "\nAvg:%d, Intg:%dus\n" NRM, Avg, Intg), \
				 sprintf(Thunk, "A%d-I%d", Avg, Intg/1000);
	tSpectroPixels *Spectro;
	int Size=0;
	
	OceanHDX_SetScansToAverage(Avg=1); 
	OceanHDX_SetIntegrationTime(Intg=MinIntg); 
	DISP;
	Spectro=OceanHDX_GetImmediateSpectrum(&Size); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 0, Intg);
	
	OceanHDX_SetScansToAverage(Avg=100);
	DISP;
	Spectro=OceanHDX_GetImmediateSpectrum(&Size); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 0, Intg);
	
	OceanHDX_SetScansToAverage(Avg=10);
	OceanHDX_SetIntegrationTime(Intg=10*1000); 
	DISP;
	Spectro=OceanHDX_GetImmediateSpectrum(&Size); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 0, Intg);
	
	OceanHDX_SetScansToAverage(Avg=1);
	OceanHDX_SetIntegrationTime(Intg=100*1000);
	DISP;
	Spectro=OceanHDX_GetImmediateSpectrum(&Size); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 0, Intg);
	
	OceanHDX_SetScansToAverage(Avg=10);
	OceanHDX_SetIntegrationTime(Intg=10*1000);
	DISP; strcat(Thunk, "-RAW");
	Spectro=OceanHDX_GetRawSpectrumWithMetaData(0, &Size); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 0, Intg);

	DISP; strcat(Thunk, "-M8-RAW");
	Spectro=OceanHDX_GetRawSpectrumWithMetaData(8, &Size); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 8, Intg);
	
	DISP; strcat(Thunk, "-M15-RAW");
	Spectro=OceanHDX_GetRawSpectrumWithMetaData(15, &Size); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 15, Intg);
	DISP; strcat(Thunk, "-M15-BXC");
	OceanHDX_BoxcarAvg(Spectro, 15); OceanHDX_SaveArray(1, Thunk, Spectro, Size, 1, Intg);
	
Skip:
	Size=MinIntg=MaxIntg=StepIntg=0;					// To avoid 'unused' warning
	UsbInfo=Alias=Serial=Firmware=NULL; Alias=UsbInfo;	// Same
	printf("=============================== Closing =======================================\n");
	OceanHDX_Close();
	if (Ret) printf(BLD RED "[Error condition present]\n" NRM);
	return Ret;
}
#endif // STANDALONE_TEST_PROG

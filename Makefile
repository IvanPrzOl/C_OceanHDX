ocean = OceanHDX

REMOTE?=0	# This is when running test progs from remote system via ssh tunnels. Also use "make REMOTE=1"
COPT = -ggdb -O0 -Wall -std=gnu11 -fdiagnostics-color=always -DUSE_COLOR_MSG -DREMOTE=${REMOTE}
CC=gcc
TP=-DSTANDALONE_TEST_PROG

$(ocean): $(ocean).c $(ocean)_QuickTest.c $(ocean)_Calib.c
	$(CC) $(COPT) -Wno-unused-function    $< $(TP)          -o $@           -lusb-1.0	# Compile as standalone test program
	$(CC) $(COPT) -Wno-unused-function    $< $@_QuickTest.c -o $@_QuickTest -lusb-1.0
	$(CC) $(COPT) -Wno-unused-function    $< $@_Calib.c     -o $@_Calib     -lusb-1.0 -lgsl	# Requires: sudo aptitude install gsl-bin libgsl-dev

install:
	sudo aptitude install gsl-bin libgsl-dev
	sudo cp -i 10-oceanoptics.rules /etc/udev/rules.d/	# Note, the original file provided by Ocean Optics has a bug

clean:
	rm -f $(ocean) *_QuickTest *_Calib core* *.o

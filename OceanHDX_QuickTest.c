#include <stdlib.h>
#include <stdio.h>
#include <iso646.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>	// sleep()
       
#include "OceanHDX.h"
#include "UseColors.h"

#define SYNTAX 	fprintf(stderr, "%s [-Aavg] [-Iintg] [-H=host] [-Pport] [-USB]: acquire a single scan on OceanHDX spectrometer via ethernet\n"\
					"\t-Aavg\tHardware averages avg scans (default %d)\n"\
					"\t-Bavg\tBoxcar averages avg scans (default 1, range 1..15)\n"\
					"\t-Iintg\tIntegration time in ms (default %d)\n"\
					"\t-auto\tAuto-adjust integration time (incompatible with dark)\n"\
					"\t-dark\tTake a dark after scan (incompatible with auto-adjust integration time)\n"\
					"\t-H=host\tHostname or IP address (default %s)\n"\
					"\t-Pport\tPort number (default %d)\n"\
					"\t-USB\tUse USB device %04X:%04X instead of ethernet\n"\
					, argv[0], Avg, Intg, IP, Port, VID, PID);


int main(int argc, const char* argv[]) {
	int Ret=0, Shift=0, Dark=0, Auto=0, Bxc=0;
	char IP[256]="192.168.0.4";	// Set by DHCP
	int Port=57357;
	const int VID=0x2457, PID=0x2003;	// Use lsusb to get this info
	int USB=0;
	unsigned int Avg=1, Intg=1000, MinIntg, MaxIntg, StepIntg;
	errno=0;
	
	for (int i=1; i<argc; i++) {	// Assumed to be first arguments
			 if (0==strncmp(argv[i], "-H=",3)) { Shift++; strcpy(IP, argv[i]+3); printf("Using address %s\n", IP); }
		else if (0==strncmp(argv[i], "-P", 2)) { Shift++; Port=atoi(argv[i]+2); }
		else if (0==strncmp(argv[i], "-A", 2)) { Shift++; Avg =atoi(argv[i]+2); }
		else if (0==strncmp(argv[i], "-B", 2)) { Shift++; Bxc =atoi(argv[i]+2); if (Bxc<0) Bxc=0; if (Bxc>15) Bxc=15; }
		else if (0==strncmp(argv[i], "-I", 2)) { Shift++; Intg=atoi(argv[i]+2)*1000; }
		else if (0==strcmp(argv[i], "-auto" )) { Shift++; Auto=1; if (Dark) {SYNTAX; return 1;}}
		else if (0==strcmp(argv[i], "-dark" )) { Shift++; Dark=1; if (Auto) {SYNTAX; return 1;}}
		else if (0==strcmp(argv[i], "-USB" ))  { Shift++; USB=1; }
		else { SYNTAX; return 1; }
	}
	
	OceanHDX_Debug(D_NONE);		// Print less detailed info

	if (USB and (Ret=OceanHDX_InitUSB(VID, PID)) or
	            (Ret=OceanHDX_InitEth(IP, Port))) return Ret;
	
	char* Serial=OceanHDX_GetSerial();
	if (OceanHDX_GetRecoveryStatus()) goto Skip;	// Better stop operation from here. Risky otherwise
	OceanHDX_SetCurrentTime();
	
	if ( 20!=OceanHDX_GetTestPayload( 20)) { Ret=2; goto Skip; }
	
	// Various statuses
	MinIntg =OceanHDX_GetMinIntegrationTime();	if (Intg<MinIntg) Intg=MinIntg; // in us
	MaxIntg =OceanHDX_GetMaxIntegrationTime();	if (Intg>MaxIntg) Intg=MaxIntg;
	StepIntg=OceanHDX_GetIntegrationTimeStepSize();
	printf(BLD WHT "\nIntegration: %d..%d in steps of %dms\n" NRM, MinIntg/1000, MaxIntg/1000, StepIntg/1000);
	OceanHDX_GetCoefs();
	OceanHDX_ClearBuffer();
	
//	printf("\nPress [Enter] to continue: "); getchar();
	char Thunk[80];
	#define DISP printf(BLD PRP "\nAvg:%d, Intg:%dms\n" NRM, Avg, Intg/1000), sprintf(Thunk, "A%d-I%d", Avg, Intg/1000);
	tSpectroPixels *Spectro;
	int Size=0;
	
	OceanHDX_SetScansToAverage(Avg); 
	OceanHDX_SetIntegrationTime(Intg); 
	
	double Corrected[PIXEL_NB];
	unsigned short Max;
	int Saturated;
	tSpectroPixels DarkPixels;

	if (Auto) while (1) {	// Auto-adjust integration time. This is incompatible with dark removal
		printf(BLD PRP "\n***Avg:%d, Intg:%dms\n" NRM, Avg, Intg/1000);
		Spectro=OceanHDX_GetImmediateSpectrum(&Size); 
		Saturated=OceanHDX_CorrectForNonLinearities(Spectro, PIXEL_NB, Corrected, &Max);
		if (Saturated>0  and Intg>=MinIntg*2) { OceanHDX_SetIntegrationTime(Intg=Intg/2/StepIntg*StepIntg); continue; }
		if (Max<0xFFFF/3 and Intg<=MaxIntg/2) { OceanHDX_SetIntegrationTime(Intg=Intg*2/StepIntg*StepIntg); continue; }
		
		if (Bxc>1) {
			Spectro=OceanHDX_GetRawSpectrumWithMetaData(Bxc, &Size);
			sprintf(Thunk, "%s-A%d-I%d-B%d", Serial, Avg, Intg/1000, Bxc);
			OceanHDX_BoxcarAvg(Spectro, Bxc); 
			Saturated=OceanHDX_CorrectForNonLinearities(Spectro, PIXEL_NB, Corrected, &Max);
		} else
			sprintf(Thunk, "%s-A%d-I%d", Serial, Avg, Intg/1000);
		break;
	}
	else {
		printf(BLD PRP "\nAvg:%d, Intg:%dms\n" NRM, Avg, Intg/1000);
		if (Bxc<=1)
			Spectro=OceanHDX_GetImmediateSpectrum(&Size);
		else {
			Spectro=OceanHDX_GetRawSpectrumWithMetaData(Bxc, &Size);
			OceanHDX_BoxcarAvg(Spectro, Bxc); 
		}
		
		if (Dark) {
			tSpectroPixels Pixels;
			memcpy(&Pixels, Spectro, sizeof(tSpectroPixels)); 
			printf("Ready to take dark. Press [Enter]:"); getchar();
			sprintf(Thunk, "%s-A%d-I%d-Dark", Serial, Avg, Intg/1000);
			memcpy(&DarkPixels, OceanHDX_GetImmediateSpectrum(&Size), sizeof(tSpectroPixels)); 
			OceanHDX_SaveArray(1, Thunk, &DarkPixels, Size, 0, Intg);	// Optional
			memcpy(Spectro, &Pixels, sizeof(tSpectroPixels)); 
			OceanHDX_DarkRemoval(Spectro, &DarkPixels);
		}

		Saturated=OceanHDX_CorrectForNonLinearities(Spectro, PIXEL_NB, Corrected, &Max);
		if (Bxc<=1) sprintf(Thunk, "%s-A%d-I%d%s",     Serial, Avg, Intg/1000,      Dark?"-ND":"");
		else        sprintf(Thunk, "%s-A%d-I%d-B%d%s", Serial, Avg, Intg/1000, Bxc, Dark?"-ND":"");
	}
	
	OceanHDX_SaveArray(1, Thunk, Spectro, Size, 0, Intg);
	
Skip:
	Size=MinIntg=MaxIntg=StepIntg=0;	// To avoid 'unused' warning
	printf("================== Closing ==================\n");
	OceanHDX_Close();
	return Ret;
}


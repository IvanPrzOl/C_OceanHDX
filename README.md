# C_OceanHDX

Simple C code to pilot an Ocean Optics OceanHDX miniature spectrometer.

I got pissed when none of the official libraries (SeaBreeze or OmniDriver) would support this spectrometer, so I wrote my own based on the specs found in "Ocean HDX Firmware and Advanced Communications Rev2.pdf".

There are no dependencies, just plain ANSI C, but the calibration code uses the GNU Scientific Library.
Applies to OCEAN-HDX-UV-VIS, OCEAN-HDX-VIS-NIR and OCEAN-HDX-XR (so far it's agnostic as to the model).
This Linux driver supports Ethernet and USB.
Adapting this code for Windows should be easy as well, just the TCP code needs to change, the USB should work as is.

There are 4 programs:
 - OceanHDX is the test program of the library and tests most of the options
 - OceanHDX_QuickTest takes a simple spectrum (with some options)
 - OceanHDX_Calib allows for calibration of the wavelength coefficients if you have a Ocean Optics HG-1 or HG-2 calibration source
 - SpectroVisu is a Windows program to visualize the spectra saved by the above programs

More info with '-h' or going to http://www.gdargaud.net/Hack/OceanSpectro.html

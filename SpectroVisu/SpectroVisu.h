/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PNL                              1       /* callback function: cb_Panel */
#define  PNL_DISPLAY                      2       /* control type: ring, callback function: cb_Display */
#define  PNL_LINES                        3       /* control type: radioButton, callback function: cb_Lines */
#define  PNL_LOG                          4       /* control type: radioButton, callback function: cb_LogScale */
#define  PNL_SATURATION                   5       /* control type: LED, callback function: (none) */
#define  PNL_PEAK                         6       /* control type: color, callback function: (none) */
#define  PNL_X                            7       /* control type: color, callback function: (none) */
#define  PNL_Y                            8       /* control type: numeric, callback function: (none) */
#define  PNL_GRAPH                        9       /* control type: graph, callback function: cb_Graph */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK cb_Display(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Graph(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Lines(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_LogScale(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
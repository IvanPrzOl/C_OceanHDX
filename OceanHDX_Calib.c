#include <stdlib.h>
#include <stdio.h>
#include <iso646.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>		// isdigit()
#include <unistd.h>		// sleep()
#include <math.h>
#include <gsl/gsl_multifit.h>			// GNU Scientific Library for multiple linear regression
#include <gsl/gsl_statistics_double.h>	// For gsl_stats_correlation()

#include "OceanHDX.h"
#include "UseColors.h"

typedef struct sModel {
	char Model[80];
	int RangeMin, RangeMax;	// in nm
	int EntranceSlit;		// in um. 10, 25, 50, 100 or 200 um width options
	float I, C1, C2, C3;	// Wavelength coefficients. Those are approximate hardcoded values and a security in case we write wrong values
} tModel;
tModel Models[]={	{"OCEAN-HDX-UV-VIS",	200,  800, 10}, // FIXME: I don't know the wavelength coeffs for this model
					{"OCEAN-HDX-VIS-NIR",	350,  925, 10, 344.229, 0.362076, -4.50373e-05, 3.10282e-09},
					{"OCEAN-HDX-XR",		200, 1100, 10, 193.018, 0.557475, -6.31192e-05, 4.1691e-09}
				};

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Returns index of closest wavelength in the array
/// HIRET	-1 if out of range.
///////////////////////////////////////////////////////////////////////////////
static int FindClosestWLIndex(double WL) {
	int i;
	for (i=0; i<PIXEL_NB; i++) {
		if (WL<OceanHDX_Wavelengths[i]) break;
	}
	if (i==0       ) return -1;		// Out of range
	if (i==PIXEL_NB) return -1;		// Not found
	// Return closest
	if (WL<=(OceanHDX_Wavelengths[i]+OceanHDX_Wavelengths[i-1])/2) return i-1;
	return i;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Look for a peak near a certain Index and within a certain wavelength range
/// HIPAR	Array/Array of intensities (size PIXEL_NB)
/// HIPAR	Index/Index around which to start the search
/// HIPAR	SearchRange/Number of nm within which to search on each side.
/// HIPAR	SearchRange/Since OceanHDX_Wavelengths[] is non-linear, the search range in terms of index values is not that same depending on the wavelength
/// HIPAR	Print/1 to print the intensity values (the main peak in white)
/// HIPAR	kmin/optional return value for the min index (pass NULL if not interested)
/// HIPAR	kmax/optional return value for the max index. Those are used to align the results through multiple calls
///////////////////////////////////////////////////////////////////////////////
static int IndexOfPeakAroundIndex(unsigned short Array[], int Index, double SearchRange, int Print, int *kmin, int *kmax) {
	int i=Index, Idx=0, iMin=Index, iMax=Index;
	unsigned short Max=0;
	while (i>0 and OceanHDX_Wavelengths[i]>=OceanHDX_Wavelengths[Index]-SearchRange) {
		if (Max<Array[i]) Max=Array[Idx=i];
		iMin=i--;
	};
	i=Index;
	while (i<PIXEL_NB-1 and OceanHDX_Wavelengths[i]<=OceanHDX_Wavelengths[Index]+SearchRange) {
		if (Max<Array[i]) Max=Array[Idx=i];
		iMax=i++;
	}
	if (Print) 
		for (i=iMin; i<=iMax; i++) 
			printf(" %s%5d%s", i==Idx?BLD WHT:"", Array[i], i==Idx?NRM:"");	// Same number than on (***)
	if (kmin!=NULL) *kmin=iMin;
	if (kmax!=NULL) *kmax=iMax;
	return Idx;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Parabola fit based on 3 points around the X2/Y2 peak. Approximates a gaussian
/// HIFN	Return the X center with Y2 being the highest value
/// HIRET	The X position of the interpolated peak
///////////////////////////////////////////////////////////////////////////////
static double FindPeakOf3(double X1, double Y1, double X2, double Y2, double X3, double Y3) {
//	Optional but unnecessary intermediate steps
//	double X    =          (X1-X2) *          (X3-X1) *          (X2-X3);
//	double Alpha=(      X3*(Y2-Y1) +       X2*(Y1-Y3) +       X1*(Y3-Y2))/D;
//	double Beta =(   X3*X3*(Y1-Y2) +    X2*X2*(Y3-Y1) +    X1*X1*(Y2-Y3))/D;
//	double Gamma=(Y1*X2*X3*(X2-X3) + Y2*X3*X1*(X3-X1) + Y3*X1*X2*(X1-X2))/D;
//	double b    =-Beta/(2*Alpha);
	return - (X3*X3*(Y1-Y2) + X2*X2*(Y3-Y1) + X1*X1*(Y2-Y3)) / (X3*(Y2-Y1) + X2*(Y1-Y3) + X1*(Y3-Y2)) / 2;
}


#define SYNTAX 	fprintf(stderr, "%s [-USB] [-H=host] [-Pport] [-Aavg|-auto] [-Iintg] [-F] [-SR=val] [-HG1] [-C=model|-C{0,1,2,3}=val] [-R[R]]\n"\
					"Perform the calibration of an OceanHDX spectrometer with a HG-1/HG-2 mercury calibration source\n"\
					"Will find the coefficients in the pixel→wavelength formula: λ=I+C1.p+C2.p^2+C3.p^3 and optionally write them to the firmware\n"\
					"Configuration options:\n"\
					"\t-USB\tUse USB device %04X:%04X instead of ethernet\n"\
					"\t-H=host\t"	"Hostname or IP address (default %s)\n"\
					"\t-Pport\t"	"Port number (default %d)\n"\
					"Processing options:\n"\
					"\t-Aavg\t"		"Hardware averages avg scans (default %d, recommanded higher value)\n"\
					"\t-Iintg\t"	"Integration time in ms (default %d or min allowed (usually 6ms))\n"\
					"\t-auto\t"		"Auto-adjust integration time (recommanded)\n"\
					"\t-F\t"		"Improve peak search by using a parabola fit (small increase in precision)\n"\
					"\t-SR=val\t"	"Search range on each side of a peak, in nm (default %.1fnm)\n"\
					"\t-HG1\t"		"Use HG-1 calibration source (default is to use HG-%d)\n"\
					"Firmware setting options:\n"\
					"\t-C=%s\t"		"Set default %s wavelength coefficients into the spectro and exit: I=%g C1=%g C2=%g C3=%g\n"\
					"\t-C=%s\t"		"Set default %s wavelength coefficients into the spectro and exit: I=%g C1=%g C2=%g C3=%g\n"\
					"\t-C=%s\t"		"Set default %s wavelength coefficients into the spectro and exit: I=%g C1=%g C2=%g C3=%g\n"\
					"\t-C0=val\t"	"Set wavelength coefficients I to 'val' and exit\n"\
					"\t-C1=val\t"	"Set wavelength coefficients C1 to 'val' and exit\n"\
					"\t-C2=val\t"	"Set wavelength coefficients C2 to 'val' and exit\n"\
					"\t-C3=val\t"	"Set wavelength coefficients C3 to 'val' and exit\n"\
					"\t-R\t"		"Reset the spectro and exit. The DHCP client will restart, so wait a minute before attempting to reconnect.\n"\
					"\t-RR\t"		"Reset and restore factory settings, then exit.\n"\
					, argv[0], VID, PID, IP, Port, Avg, Intg/1000, SR, HG,\
					Models[0].Model+10, Models[0].Model+10, Models[0].I, Models[0].C1, Models[0].C2, Models[0].C3, \
					Models[1].Model+10, Models[1].Model+10, Models[1].I, Models[1].C1, Models[1].C2, Models[1].C3, \
					Models[2].Model+10, Models[2].Model+10, Models[2].I, Models[2].C1, Models[2].C2, Models[2].C3  \
				)


///////////////////////////////////////////////////////////////////////////////
int main(int argc, const char* argv[]) {
	int Ret=0, Shift=0, Auto=0, SetHardcoded=-1, ParaFit=0, HG=2, Reset=0;
	static char IP[256]="192.168.0.4";	// Set by DHCP
	static int Port=57357;
	const int VID=0x2457, PID=0x2003;	// Use lsusb to get this info
	int USB=0;
	unsigned int Avg=1, Intg=1000, MinIntg, MaxIntg, StepIntg;
	#define UNSET 1e300
	double SetC0=UNSET, SetC1=UNSET, SetC2=UNSET, SetC3=UNSET;
	double SR=4.;		// Search range on each side in nm
	errno=0;
	
	///////////////////// Command-line parameter handling /////////////////////
	for (int i=1; i<argc; i++) {	// Assumed to be first arguments
		     if (0==strcmp(argv[i], "-USB" ))   { Shift++; USB=1; printf("Looking for USB\n"); }
		else if (0==strncmp(argv[i], "-H=",3))  { Shift++; strcpy(IP, argv[i]+3); printf("Using address %s\n", IP); }
		else if (0==strncmp(argv[i], "-P", 2))  { Shift++; Port=atoi(argv[i]+2); }
		else if (0==strncmp(argv[i], "-A", 2))  { Shift++; Avg =atoi(argv[i]+2); }
		else if (0==strncmp(argv[i], "-I", 2))  { Shift++; Intg=atoi(argv[i]+2)*1000; }
		else if (0==strcmp(argv[i], "-auto" ))  { Shift++; Auto=1; }
		else if (0==strcmp(argv[i], "-F" ))     { Shift++; ParaFit=1; }
		else if (0==strcmp(argv[i], "-R" ))     { Shift++; Reset=1; }
		else if (0==strcmp(argv[i], "-RR" ))    { Shift++; Reset=2; }
		else if (0==strncmp(argv[i], "-SR=",4)) { Shift++; SR=atof(argv[i]+4); }
		else if (0==strncmp(argv[i], "-HG", 3)) { Shift++; HG=atoi(argv[i]+3); }
		else if (0==strncmp(argv[i], "-C0=",4)) { Shift++; SetC0=atof(argv[i]+4); }
		else if (0==strncmp(argv[i], "-C1=",4)) { Shift++; SetC1=atof(argv[i]+4); }
		else if (0==strncmp(argv[i], "-C2=",4)) { Shift++; SetC2=atof(argv[i]+4); }
		else if (0==strncmp(argv[i], "-C3=",4)) { Shift++; SetC3=atof(argv[i]+4); }
		else if (0==strncmp(argv[i], "-C=",3)) { 
			Shift++; 
			     if (0==strcmp(argv[i]+3, Models[0].Model+10)) SetHardcoded=0; 
			else if (0==strcmp(argv[i]+3, Models[1].Model+10)) SetHardcoded=1; 
			else if (0==strcmp(argv[i]+3, Models[2].Model+10)) SetHardcoded=2; 
			else { SYNTAX; return 1; }
		} else   { SYNTAX; return 1; }
	}
	
	////////////////////////// Spectrometer initialization ////////////////////
	OceanHDX_Debug(D_NONE);		// Print less detailed info

	if (USB) { 
		if ((Ret=OceanHDX_InitUSB(VID, PID))) return Ret;
	} else {
		if ((Ret=OceanHDX_InitEth(IP, Port))) return Ret;
	}
	
	char* Serial=OceanHDX_GetSerial();
	if (OceanHDX_GetRecoveryStatus()) goto Skip;	// Better stop operation from here. Risky otherwise
	OceanHDX_SetCurrentTime();
	
	if ( 20!=OceanHDX_GetTestPayload( 20)) { Ret=2; goto Skip; }
	
	////////////////////////////// Resets /////////////////////////////////////
	if (Reset==1) { OceanHDX_Reset();        printf(BLD WHT "Resetting...\nWait a minute before attempting to reconnect\n" NRM); goto Skip; }
	if (Reset==2) { OceanHDX_ResetRestore(); printf(BLD WHT "Resetting...\nWait a minute before attempting to reconnect\n" NRM); goto Skip; }	// This will erase many settings, use with care
	
	///////////// Direct setting of the wavelength coefficients ///////////////
	OceanHDX_GetCoefs();
	if (SetHardcoded>=0) {
		OceanHDX_SetWavelengthCoefs(Models[SetHardcoded].I, 
									Models[SetHardcoded].C1, 
									Models[SetHardcoded].C2, 
									Models[SetHardcoded].C3);
		goto Skip;
	}

	if (SetC0!=UNSET) OceanHDX_SetWavelengthCoef(0, SetC0);
	if (SetC1!=UNSET) OceanHDX_SetWavelengthCoef(1, SetC1);
	if (SetC2!=UNSET) OceanHDX_SetWavelengthCoef(2, SetC2);
	if (SetC3!=UNSET) OceanHDX_SetWavelengthCoef(3, SetC3);
	if (SetC0!=UNSET or SetC1!=UNSET or SetC2!=UNSET or SetC3!=UNSET) goto Skip;
	
	///////////////////////////// Get/set some parameters /////////////////////
	MinIntg =OceanHDX_GetMinIntegrationTime();	if (Intg<MinIntg) Intg=MinIntg; // in us
	MaxIntg =OceanHDX_GetMaxIntegrationTime();	if (Intg>MaxIntg) Intg=MaxIntg;
	StepIntg=OceanHDX_GetIntegrationTimeStepSize();
	printf(BLD WHT "\nIntegration: %d..%dms in steps of %dms\n" NRM, MinIntg/1000, MaxIntg/1000, StepIntg/1000);
	
	tSpectroPixels *Spectro;
	int Size=0;
	
	OceanHDX_SetScansToAverage(Avg); 
	OceanHDX_SetIntegrationTime(Intg); 
	OceanHDX_ClearBuffer();
	
	double Corrected[PIXEL_NB];	// We aren't using it here. We just call the function to get the number of saturated pixels
	unsigned short Max;
	int Saturated;

	////////////////////////////// Get a spectrogram //////////////////////////
	if (Auto) while (1) {	// Auto-adjust integration time.
		printf(BLD PRP "\nSearching for optimal integration rate: %dms\n" NRM, Intg/1000);
		Spectro=OceanHDX_GetImmediateSpectrum(&Size); 
		Saturated=OceanHDX_CorrectForNonLinearities(Spectro, PIXEL_NB, Corrected, &Max);
		if (Saturated>0  and Intg>=MinIntg*2) { OceanHDX_SetIntegrationTime(Intg=Intg/2/StepIntg*StepIntg); continue; }
		if (Max<0xFFFF/3 and Intg<=MaxIntg/2) { OceanHDX_SetIntegrationTime(Intg=Intg*2/StepIntg*StepIntg); continue; }
		break;
	}
	else {
		printf(BLD PRP "\nAvg:%d, Intg:%dms\n" NRM, Avg, Intg/1000);
		Spectro=OceanHDX_GetImmediateSpectrum(&Size);
		Saturated=OceanHDX_CorrectForNonLinearities(Spectro, PIXEL_NB, Corrected, &Max);
		if (Saturated>0) { 
			printf(BLD RED "There are %d saturated pixels. Use a lower integration or a filter. Aborting\n" NRM, Saturated); goto Skip; }
	}
	
	/////////////////////// Optional: saving the data /////////////////////////
	char Thunk[80];
	sprintf(Thunk, "%s-A%d-I%d", Serial, Avg, Intg/1000);
	OceanHDX_SaveArray(1, Thunk, Spectro, Size, 0, Intg);

	// True expected wavelengths of peaks of calibration sources
	// ...for Ocean Optics HG-1 mercury calibration source
	double HG1[]={253.652, 296.728, 302.150, 313.155, 334.148, 365.015, 404.656, 407.783, 435.833, 546.074, 576.960, 579.066, 696.543, 706.722, 727.294, 738.393, 751.47};
	// ...for Ocean Optics HG-2 mercury-argon calibration source
	double HG2[]={253.652, 296.728, 302.150, 313.155, 334.148, 365.015, 404.656, 407.783, 435.833, 546.074, 576.960, 579.066, 696.543, 706.722, 714.704, 727.294, 738.393, 750.387, 763.511, 772.376, 794.818, 800.616, 811.531, 826.452, 842.465, 852.144, 866.794, 912.297, 922.450};
	double *TrueWL;
	int NbT=0;
	switch (HG) {
		case 1:	TrueWL=HG1; NbT=sizeof(HG1)/sizeof(double); break;
		case 2:	TrueWL=HG2;	NbT=sizeof(HG2)/sizeof(double); break;
		default: printf(BLD RED "Unkown calibration source\n" NRM); goto Skip;
	}
	printf("Number of calibration peaks to search for: %d\n", NbT);	
	int i, j, k;
	
	// Find which calibration peaks should be within the range of our spectrometer model
	int StartIdx=-1, EndIdx=-1;
	printf(BLD WHT "\nCurrent spectrometer wavelength range: %.3f..%.3fnm, resolution: %.3f..%.3fnm\n", 
			OceanHDX_Wavelengths[0],         OceanHDX_Wavelengths[PIXEL_NB-1],
			OceanHDX_Wavelengths[1]         -OceanHDX_Wavelengths[0],
			OceanHDX_Wavelengths[PIXEL_NB-1]-OceanHDX_Wavelengths[PIXEL_NB-2]);
	for (i=0; i<NbT; i++) {
		if (TrueWL[i]>OceanHDX_Wavelengths[0] and StartIdx==-1) StartIdx=i;
		if (TrueWL[i]<OceanHDX_Wavelengths[PIXEL_NB-1]        )   EndIdx=i;
	}

	printf(BLD WHT "Looking for peaks among %d Hg peaks (%d within range):\n",
					NbT, EndIdx-StartIdx+1);
	printf("Index: Intensities\tTrueWL/CloseWL/InterpWL, ExpectedIdx/Relative/Interp\n" NRM);
	// First do a dry run to figure out the alignments
	int kmin=0, kmax=0, minmin=INT_MIN, maxmax=INT_MIN;
	for (i=StartIdx; i<=EndIdx; i++) {
		j=FindClosestWLIndex(TrueWL[i]);
		k=IndexOfPeakAroundIndex(Spectro->Spectrum, j, SR, 0, &kmin, &kmax);
		if (minmin<j-kmin) minmin=j-kmin;
		if (maxmax<kmax-j) maxmax=kmax-j;
//		printf("%2d: %d %d\n", i, j-kmin, kmax-j);
	}
//	printf("minmin:%d, maxmax:%d\n", minmin, maxmax);
	// Then do a 2nd run and align the expected peaks in the middle, leaving blanks on the sides if necessary
	for (i=StartIdx; i<=EndIdx; i++) {
		printf("%2d:", i);
		j=FindClosestWLIndex(TrueWL[i]);
		k=IndexOfPeakAroundIndex(Spectro->Spectrum, j, SR, 0, &kmin, &kmax);
		for (int z=j-kmin; z<minmin; z++) printf(" %5s", "");	// Same number than on (***)
		k=IndexOfPeakAroundIndex(Spectro->Spectrum, j, SR, 1, NULL, NULL);
		for (int z=kmax-j; z<maxmax; z++) printf(" %5s", "");
		double NM=FindPeakOf3(OceanHDX_Wavelengths[k-1], Spectro->Spectrum[k-1],
							  OceanHDX_Wavelengths[k  ], Spectro->Spectrum[k  ],
							  OceanHDX_Wavelengths[k+1], Spectro->Spectrum[k+1]);	// This returns the optimal wavelength, but we need the pixel
		double p=FindPeakOf3(k-1, Spectro->Spectrum[k-1],
							 k  , Spectro->Spectrum[k  ],
					   k+1, Spectro->Spectrum[k+1]);	// Less accurate than above because of non-linearities
		
		printf(" %.3f/%.3f/%.3fnm, %d/%+d/%.1f\n", TrueWL[i], OceanHDX_Wavelengths[j], NM, j, k-j, p);
	}
	
	//////////////////////// Removal of outliers //////////////////////////////
	int Exclude[256], NbExcl=0;
	char Str[256];
	printf(BLD WHT "Please list indices of *all* lines with outlier peaks and/or saturated pixels. They will be excluded from computation. Use [Space] to separate, then press [Enter]: " GRN);
	fflush(0);	// This is necessary when piping to tee and aha
	if (fgets(Str, 256, stdin));	// To silence the warn_unused_result
	printf(NRM);
	int Count=EndIdx-StartIdx+1;
	char *P=strtok(Str, " ,");
	do {	
		if (!isdigit(*P)) break;
		Exclude[NbExcl++]=atoi(P);
		P=strtok(NULL, " ,");
		Count--;
	} while (P!=NULL);
	
	/////////////////////// Perform multilinear fit ///////////////////////////
	double chisq;
	gsl_matrix *X, *cov;
	gsl_vector *y, *c;
	
	X  = gsl_matrix_alloc(Count, 4);
	y  = gsl_vector_alloc(Count);
	c  = gsl_vector_alloc(4);
	cov= gsl_matrix_alloc(4, 4);
	int Cnt=0;	// Effective number of peaks after removal of outliers
	
	printf("Regression analysis table:\n"
			"Index\tTrueWL\tPix\tPix^2\tPix^3\n");
	for (i=StartIdx; i<=EndIdx; i++) {
		for (j=0; j<NbExcl; j++) if (i==Exclude[j]) break;
		if (j<NbExcl and i==Exclude[j]) continue;
		
		j=FindClosestWLIndex(TrueWL[i]);
		k=IndexOfPeakAroundIndex(Spectro->Spectrum, j, SR, 0, NULL, NULL);
		double p=FindPeakOf3(k-1, Spectro->Spectrum[k-1],
							 k  , Spectro->Spectrum[k  ],
							 k+1, Spectro->Spectrum[k+1]);	// Less accurate than above because of non-linearities

		gsl_matrix_set (X, Cnt, 0, 1.0);
		if (ParaFit) {
			printf("%d\t%.3f\t%6.1f\t%9.1f\t%12.1f\n", i, TrueWL[i], p, p*p, p*p*p);
			gsl_matrix_set (X, Cnt, 1, p);
			gsl_matrix_set (X, Cnt, 2, p*p);
			gsl_matrix_set (X, Cnt, 3, p*p*p);
		} else {
			printf("%d\t%.3f\t%d\t%d\t%lld\n",     i, TrueWL[i], k, k*k, (long long)k*k*k);
			gsl_matrix_set (X, Cnt, 1, k);
			gsl_matrix_set (X, Cnt, 2, k*k);
			gsl_matrix_set (X, Cnt, 3, (double)k*k*k);	// Otherwise overflow
		}
		gsl_vector_set (y, Cnt++, TrueWL[i]);
	}
	printf("\n");
	
	gsl_multifit_linear_workspace* work = gsl_multifit_linear_alloc (Count, 4);
	gsl_multifit_linear (X, y, c, cov, &chisq, work);	// NOTE: this will dump core if the user's exclusions of outliers/saturations are wrong
	gsl_multifit_linear_free (work);
	
	#define C(i)     (gsl_vector_get(c,(i)))
	#define COV(i,j) (gsl_matrix_get(cov,(i),(j)))
	
	printf (BLD GRN "Best fit    : λ = %g %+g.p %+g.p^2 %+g.p^3\n" NRM, C(0), C(1), C(2), C(3));
	printf (BLD WHT "Previous was: λ = %g %+g.p %+g.p^2 %+g.p^3\n" NRM, 
			OceanHDX_Wavelength.Coefs[0], OceanHDX_Wavelength.Coefs[1], OceanHDX_Wavelength.Coefs[2], OceanHDX_Wavelength.Coefs[3]);

	printf ("Covariance matrix:\n");
	printf ("[ %+.5e, %+.5e, %+.5e, %+.5e  \n", COV(0,0), COV(0,1), COV(0,2), COV(0,3));
	printf ("  %+.5e, %+.5e, %+.5e, %+.5e  \n", COV(1,0), COV(1,1), COV(1,2), COV(1,3));
	printf ("  %+.5e, %+.5e, %+.5e, %+.5e  \n", COV(2,0), COV(2,1), COV(2,2), COV(2,3));
	printf ("  %+.5e, %+.5e, %+.5e, %+.5e ]\n", COV(3,0), COV(3,1), COV(3,2), COV(3,3));
	printf ("Chi^2 = %g\n", chisq);
	
	////////////////////// Optional: R Square /////////////////////////////////
	int mn=0;
	double *data1, *data2;
	data1=calloc(Count, sizeof(double));
	data2=calloc(Count, sizeof(double));
	for (i=StartIdx; i<=EndIdx; i++) {
		for (j=0; j<NbExcl; j++) if (i==Exclude[j]) break;
		if (j<NbExcl and i==Exclude[j]) continue;
		j=FindClosestWLIndex(TrueWL[i]);
		k=IndexOfPeakAroundIndex(Spectro->Spectrum, j, SR, 0, NULL, NULL);
		double p=FindPeakOf3(k-1, Spectro->Spectrum[k-1],
							 k  , Spectro->Spectrum[k  ],
							 k+1, Spectro->Spectrum[k+1]);	// Less accurate than above because of non-linearities
		if (ParaFit) data2[mn]=C(0)+p*(C(1)+p*(C(2)+p*C(3)));
		else         data2[mn]=C(0)+k*(C(1)+k*(C(2)+k*C(3)));
		data1[mn++]=TrueWL[i];
	}
	if (Count!=mn) printf(BLD RED "Error: Count:%d != mn:%d\n" NRM, Count, mn);
	printf (BLD GRN "R^2 = %.9f (correlation coefficient)\n" NRM, gsl_stats_correlation(data1, 1, data2, 1, Count));
	
	///////////////////// Write to firmware ///////////////////////////////////
	printf(BLD WHT "Would you like to validate those results and write them to the spectro's firmware (you can always revert with -C) ? [y/N] " NRM);
	fflush(0);
	if ('Y'==toupper(getchar()))
		if (0==OceanHDX_SetWavelengthCoefs(C(0), C(1), C(2), C(3)))
			printf(BLD GRN "Your spectrometer is now calibrated !\n"
							"Running this again with reduced -SR and/or increased -A may slightly improve the precision.\n" NRM);
		
	gsl_matrix_free (X);
	gsl_vector_free (y);
	gsl_vector_free (c);
	gsl_matrix_free (cov);
	free(data1);
	free(data2);
	
Skip:
	Size=MinIntg=MaxIntg=StepIntg=0;	// To avoid 'unused' warning
	printf("================== Closing ==================\n");
	OceanHDX_Close();
	fflush(0);
	return Ret;
}
